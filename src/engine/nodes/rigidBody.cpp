/*! \file rigidBody.cpp
 * \author Giovanni Aleman
 *
 ******************************************************************************
 Copyright (c) 2016-2019 Giovanni Aleman

 This Source Code Form is subject to the terms of the Mozilla Public
 License, v. 2.0. If a copy of the MPL was not distributed with this
 file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************
 */


#include "rigidBody.h"

/*! \brief RigidBody constructor
 *
 * \param (const Vec3 &) pos - The position of the RigidBody
 * \param (const Vec3 &) min - The min of the RigidBody's bounding box
 * \param (const Vec3 &) max - The max of the RigidBody's bounding box
 * \param (std::string) tag - The tag of the RigidBody
 */
viv::RigidBody::RigidBody(bool useGravity, const Vec3 &pos, const Vec3 &min, const Vec3 &max, std::string tag) : CollisionBody(min, max, pos, tag)
{
	this->useGravity = useGravity;
}

/*! \brief RigidBody copy constructor
 *
 * \param (const RigidBody&) r - The RigidBody to copy
 */
viv::RigidBody::RigidBody(const RigidBody &r) : CollisionBody(r)
{
	this->vel = r.vel;
	this->force = r.force;
	this->aVel = r.aVel;
	this->torq = r.torq;
	this->grav = r.grav;

	this->mass = r.mass;
	this->speed = r.speed;
	this->drag = r.drag;
	this->inert = r.inert;
	this->aSpeed = r.aSpeed;
	this->aDrag = r.aDrag;

	this->maxSpeed = r.maxSpeed;
	this->maxASpeed = r.maxASpeed;
}

/*! \brief RigidBody update method
 */
void viv::RigidBody::update()
{
	CollisionBody::update();

	/* Angular */
	this->torq += -this->aVel * this->aDrag;
	Vec3 aA = this->torq / this->inert;
	this->aVel = aA * Time::deltaTime();

	if(this->maxASpeed != 0) {
		if(this->aVel.length() > this->maxASpeed)
			this->aVel = glm::normalize(this->aVel) * this->maxASpeed;
	}

	this->setRot(glm::pitch(this->aVel.x * this->aSpeed * Time::deltaTime()) * this->getRot() * glm::yaw(this->aVel.y * this->aSpeed * Time::deltaTime()) * glm::roll(this->aVel.z * this->aSpeed * Time::deltaTime()));

	/* Linear */
	this->force += -this->vel * this->drag * this->speed;

	if(this->useGravity)
		this->force += this->grav;

	Vec3 a = this->force / this->mass;
	this->vel = a * Time::deltaTime();

	if((this->vel.x < 0.05f && this->vel.x > 0.05f) && (this->vel.y < 0.05f && this->vel.y > 0.05f) && (this->vel.z < 0.05f && this->vel.z > 0.05f))
		this->vel = Vec3();

	if(this->maxSpeed != 0) {
		if(this->vel.length() > this->maxSpeed)
			this->vel = glm::normalize(this->vel) * this->maxSpeed;
	}
	this->setPos(this->getPos() + (this->vel * Time::deltaTime()));
}

/*! \brief Assignment operator
 *
 * \param (const RigidBody &) r - The RigidBody to copy
 *
 * \return (RigidBody &) This instance
 */
const viv::RigidBody & viv::RigidBody::operator=(const RigidBody &r)
{
	CollisionBody::operator=(r);

	this->vel = r.vel;
	this->force = r.force;
	this->aVel = r.aVel;
	this->torq = r.torq;
	this->grav = r.grav;

	this->mass = r.mass;
	this->speed = r.speed;
	this->drag = r.drag;
	this->inert = r.inert;
	this->aSpeed = r.aSpeed;
	this->aDrag = r.aDrag;

	this->maxSpeed = r.maxSpeed;
	this->maxASpeed = r.maxASpeed;

	return *this;
}

/*! \brief RigidBody Dtor
 */
viv::RigidBody::~RigidBody()
{

}

/*! \brief AngelScript def constructor binding
 *
 * \param (RigidBody*) self - Pointer to the RigidBody
 */
void viv::RigidBody::DefCtor(RigidBody* self)
{
	new (self) RigidBody();
}

/*! \brief AngelScript copy constructor binding
 *
 * \param (const RigidBody&) other - The default pos
 * \param (RigidBody*) self - Pointer to the RigidBody
 */
void viv::RigidBody::CopyCtor(const RigidBody &other, RigidBody* self)
{
	new (self) RigidBody(other);
}

/*! \brief AngelScript init constructor binding
 *
 * \param (Vec3 &) pos - The position of the RigidBody
 * \param (Vec3 &) min - The min of the RigidBody
 * \param (Vec3 &) max - The max of the RigidBody
 * \param (std::string) tag - The tag for the RigidBody
 * \param (RigidBody*) self - Pointer to the RigidBody
 */
void viv::RigidBody::InitCtor(bool useGravity, const Vec3 &pos, const Vec3 &min, const Vec3 &max, std::string tag, RigidBody* self)
{
	new (self) RigidBody(useGravity, pos, min, max, tag);
}

/*! \brief AngelScript destructor
 *
 * \param (RigidBody*) self - Pointer to the memory to deallocate
 */
void viv::RigidBody::Dtor(RigidBody* self)
{
	self->~RigidBody();
}
