/*! \file collisionBody.cpp
 * \author Giovanni Aleman
 *
 ******************************************************************************
 Copyright (c) 2016-2019 Giovanni Aleman

 This Source Code Form is subject to the terms of the Mozilla Public
 License, v. 2.0. If a copy of the MPL was not distributed with this
 file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************
 */

#include "collisionBody.h"

std::vector<viv::CollisionBody*> viv::CollisionBody::collisionObjects; //Static vector of all CollisionBodies

/*! \brief CollisionBody constructor
 *
 * \param (const Vec3 &) min - The min of the CollisionBody's bounding box
 * \param (const Vec3 &) max - The max of the CollisionBody's bounding box
 * \param (const Vec3 &) pos - The position of the CollisionBody
 * \param (std::string) tag - The tag of the CollisionBody
 */
viv::CollisionBody::CollisionBody(const Vec3 &min, const Vec3 &max, const Vec3 &pos, std::string tag) : Transform(pos, tag)
{
	this->localMin = min;
	this->localMax = max;

	this->globalMin = Vec3(transMat * Vec4(localMin, 0.0f));
	this->globalMax = Vec3(transMat * Vec4(localMax, 0.0f));
	this->halves = (localMax - localMin) / 2.0f;

	collisionObjects.push_back(this);
}

/*! \brief CollisionBody copy constructor
 *
 * \param (const CollisionBody &) cb - The CollisionBody to copy
 */
viv::CollisionBody::CollisionBody(const CollisionBody &cb) : Transform(cb)
{
	this->localMin = cb.localMin;
	this->localMax = cb.localMax;
	this->globalMin = cb.localMin;
	this->globalMax = cb.globalMax;
	this->halves = cb.halves;

	collisionObjects.push_back(this);
}

/*! \brief Update method
 */
void viv::CollisionBody::update()
{
	Transform::update();

	if(this->transMat != this->prevTransMat) {
		this->reorientBox();
		this->prevTransMat = this->transMat;

		for(auto i : collisionObjects) {
			if(i != this) {
				if (AABB(*i)) {
					if(this->collidingWith.find(i) == this->collidingWith.end())
						this->collidingWith.insert(i);
				}

				else {
					if(this->collidingWith.find(i) != this->collidingWith.end())
						this->collidingWith.erase(i);
				}
			}
		}
	}
}

/*! \brief Gets the CollisionBodies that this is colliding with
 *
 * \return(std::unordered_set<viv::CollisionBody*>) An unordered_set of the colliding bodies
 */
std::unordered_set<viv::CollisionBody*> viv::CollisionBody::getCollidingBodies()
{
	return collidingWith;
}

/*! \brief AABB collision method
 *
 * \param (const CollisionBody &) cb - The CollisionBody to check against
 * \return (bool) If they are colliding or not
 */
bool viv::CollisionBody::AABB(const CollisionBody &cb)
{
	if(this->globalMax.x < this->globalMin.x)
		return false;
	if(this->globalMin.x > this->globalMax.x)
		return false;

	if(this->globalMax.y < this->globalMin.y)
		return false;
	if(this->globalMin.y > this->globalMax.y)
		return false;

	if(this->globalMax.z < this->globalMin.z)
		return false;
	if(this->globalMin.z > this->globalMax.z)
		return false;

	if(doSphere)
		return Sphere(cb);

	return true;
}

/*! \brief Sphere collision method
 *
 * \param (const CollisionBody &) cb - The CollisionBody to check against
 * \return (bool) If they are colliding or not
 */
bool viv::CollisionBody::Sphere(const CollisionBody &cb)
{
	if (glm::distance(this->getPos(), cb.getPos()) > this->radius + cb.radius)
		return false;

	if(doSAT)
		return SAT(cb);

	return true;
}

/*! \brief SAT collision method
 *
 * \param (const CollisionBody &) cb - The CollisionBody to check against
 * \return (bool) If they are colliding or not
 */
bool viv::CollisionBody::SAT(const CollisionBody &cb)
{
	//TODO: Seperating Axis Test
	return true;
}

/*! \brief Reorients the bounding box
 */
void viv::CollisionBody::reorientBox()
{
	this->globalMin = Vec3(transMat * Vec4(localMin, 0.0f));
	this->globalMax = Vec3(transMat * Vec4(localMax, 0.0f));

	std::vector<Vec3> newPoints = { this->globalMin,
									Vec3(transMat * Vec4(this->localMin.x, this->localMin.y, this->localMax.z, 0.0f)),
									Vec3(transMat * Vec4(this->localMin.x, this->localMax.y, this->localMin.z, 0.0f)),
									Vec3(transMat * Vec4(this->localMax.x, this->localMin.y, this->localMin.z, 0.0f)),
									Vec3(transMat * Vec4(this->localMin.x, this->localMax.y, this->localMax.z, 0.0f)),
									Vec3(transMat * Vec4(this->localMin.x, this->localMax.y, this->localMax.z, 0.0f)),
									Vec3(transMat * Vec4(this->localMax.x, this->localMax.y, this->localMin.z, 0.0f)),
									this->globalMax };

	for(auto i : newPoints) {
		//x
		if(this->globalMax.x < i.x)
			this->globalMax.x = i.x;
		else if(this->globalMin.x > i.x)
			this->globalMin.x = i.x;

		//y
		if(this->globalMax.y < i.y)
			this->globalMax.y = i.y;
		else if(this->globalMin.y > i.y)
			this->globalMin.y = i.y;

		//z
		if(this->globalMax.z < i.z)
			this->globalMax.z = i.z;
		else if(this->globalMin.z > i.z)
			this->globalMin.z = i.z;
	}

	this->radius = (this->globalMax.x > this->globalMax.y) ? this->globalMax.x : this->globalMax.y;
	this->radius = (this->radius > this->globalMax.z) ? this->radius : this->globalMax.z;
}

/*! \brief CollisionBody equality operator
 *
 * \param (const CollisionBody &) cb - The CollisionBody to copy
 * \return (CollisionBody &) This instance
 */
const viv::CollisionBody & viv::CollisionBody::operator=(const CollisionBody &cb)
{
	Transform::operator=(cb);

	this->localMin = cb.localMin;
	this->localMax = cb.localMax;
	this->globalMin = cb.localMin;
	this->globalMax = cb.globalMax;

	return *this;
}

/*! \brief CollisionBody destructor
 */
viv::CollisionBody::~CollisionBody()
{

}

/*! \brief AngelScript def constructor binding
 *
 * \param (CollisionBody*) self - Pointer to the CollisionBody
 */
void viv::CollisionBody::DefCtor(CollisionBody* self)
{
	new (self) CollisionBody();
}

/*! \brief AngelScript copy constructor binding
 *
 * \param (const CollisionBody &) other - The default pos
 * \param (CollisionBody*) self - Pointer to the CollisionBody
 */
void viv::CollisionBody::CopyCtor(const CollisionBody &other, CollisionBody* self)
{
	new (self) CollisionBody(other);
}

/*! \brief AngelScript init constructor binding
 *
 * \param (Vec3 &) min - The min of the CollisionBody
 * \param (Vec3 &) max - The max of the CollisionBody
 * \param (Vec3 &) pos - The position of the CollisionBody
 * \param (std::string) tag - The tag for the CollisionBody
 * \param (CollisionBody*) self - Pointer to the CollisionBody
 */
void viv::CollisionBody::InitCtor(const Vec3 &min, const Vec3 &max, const Vec3 &pos, std::string tag, CollisionBody* self)
{
	new (self) CollisionBody(min, max, pos, tag);
}

/*! \brief AngelScript destructor
 *
 * \param (CollisionBody*) self - Pointer to the memory to deallocate
 */
void viv::CollisionBody::Dtor(CollisionBody* self)
{
	self->~CollisionBody();
}
