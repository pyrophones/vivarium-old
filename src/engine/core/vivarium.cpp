/*! \file vivarium.cpp
 * \author Giovanni Aleman
 *
 ******************************************************************************
 Copyright (c) 2016-2019 Giovanni Aleman

 This Source Code Form is subject to the terms of the Mozilla Public
 License, v. 2.0. If a copy of the MPL was not distributed with this
 file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************
 */

#include "vivarium.h"
#include "ui_vivarium.h"

/*! \brief Contructor for the winow
 *
 * \param (QWidget) parent - The parent to the window
 */
viv::Vivarium::Vivarium(QWidget *parent) :
	QMainWindow(parent),
	ui(new Ui::Vivarium)
{
	this->ui->setupUi(this);
	QRect screen = QApplication::desktop()->screenGeometry();
	int32_t x = (screen.width() - this->width()) / 2;
	int32_t y = (screen.height() - this->height()) / 2;
	this->move(x, y);
}

/*! \brief Override for Qt's close event
 *
 * \param (QCloseEvent*) event - The window close event
 */
void viv::Vivarium::closeEvent(QCloseEvent* event)
{
	System::shouldClose = true;
	event->accept();
}

/*! \brief Starts the system class
 *
 * \return (int32_t) The program success variable.
 */
int32_t viv::Vivarium::startSystem()
{
	if(System::init(this) != 0) {
		printf("Press enter to quit . . .");
		std::cin.get();
		return -1;
	}

	if(System::run() != 0) {
		printf("Press enter to quit . . .");
		std::cin.get();
		return -1;
	}

	if(System::end() != 0) {
		printf("Press enter to quit . . .");
		std::cin.get();
		return -1;
	}

	return 0;
}

/*! \brief Destructor for Vivarium window
 */
viv::Vivarium::~Vivarium()
{
	delete this->ui;
}
