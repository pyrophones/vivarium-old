/*! \file glFramebuffer.cpp
 * \author Giovanni Aleman
 *
 ******************************************************************************
 * Copyright (c) 2016-2019 Giovanni Aleman
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************
 */

#include "glFramebuffer.h"

/*! \brief GLFramebuffer constructor
 *
 * \param (uint32_t) width - The width of the renderbuffer / depth texture
 * \param (uint32_t) height - The height of the renderbuffer / depth texture
 * \param (bool) renderbuffer - If the framebuffer should use a renderbuffer or a texture
 */
viv::GLFramebuffer::GLFramebuffer(uint32_t rboWidth, uint32_t rboHeight, bool renderBuffer) : Framebuffer(rboWidth, rboHeight)
{
	glGenFramebuffers(1, &this->fbo);
	glBindFramebuffer(GL_FRAMEBUFFER, this->fbo);

	if(renderBuffer) {
		glGenRenderbuffers(1, &this->rbo);
		glBindRenderbuffer(GL_RENDERBUFFER, this->rbo);
		glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT24, this->rboWidth, this->rboHeight);
		glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, this->rbo);
	}

	else {
		this->depthTex = new GLTexture(rboWidth, rboHeight, 3, GL_FLOAT, GL_DEPTH_COMPONENT24, GL_DEPTH_COMPONENT, GL_CLAMP_TO_EDGE, GL_NEAREST, GL_NEAREST, nullptr, false);
		this->attachDepthStencil(this->depthTex, true, false, GL_TEXTURE_2D, 0);
	}

	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
		printf("Error: Framebuffer not complete!\n");
}

/*! \brief GLFramebuffer copy constructor
 *
 * \param (const GLFramebuffer &) gfb - The OpenGL framebuffer to copy
 */
viv::GLFramebuffer::GLFramebuffer(const GLFramebuffer &gfb) : Framebuffer(gfb)
{
	glGenFramebuffers(1, &this->fbo);

	if(gfb.depthTex == nullptr) {
		glBindFramebuffer(GL_FRAMEBUFFER, this->fbo);
		glGenRenderbuffers(1, &this->rbo);
		glBindRenderbuffer(GL_RENDERBUFFER, this->rbo);
		glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT24, this->rboWidth, this->rboHeight);
		glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, this->rbo);
	}

	else {
		this->depthTex = gfb.depthTex;
		this->attachDepthStencil(this->depthTex, true, false, GL_TEXTURE_2D, 0);
	}

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

/*! \brief Attaches a texture to the framebuffer
 *
 * \param (const Texture*) texture - The texture to attach
 * \param (uint32_t) attachmentNum - The number for the attachment
 * \param (uint32_t) type - The type of texture to attach
 * \param (uint32_t) mipLevel - the mipmap level of the attachment
 */
void viv::GLFramebuffer::attachTexture(const Texture* texture, uint32_t attachmentNum, uint32_t type, uint32_t mipLevel) const
{
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + attachmentNum, type, ((const GLTexture*)texture)->getTexture(), mipLevel);
}

/*! \brief Attaches a depth / stencil texture tothe framebuffer
 *
 * \param (const Texture*) texture - The texture to attach
 * \param (bool) depth - If the attachment is a depth attachment
 * \param (bool) stencil - If the attachment is a stencil attachment
 * \param (uint32_t) type - The type of texture to attach
 * \param (uint32_t) mipLevel - the mipmap level of the attachment
 */
void viv::GLFramebuffer::attachDepthStencil(const Texture* texture, bool depth, bool stencil, uint32_t type, uint32_t mipLevel) const
{
	//Maybe switch to enum?
	if(depth && stencil)
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, type, ((const GLTexture*)texture)->getTexture(), mipLevel);
	else if (depth && !stencil)
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, type, ((const GLTexture*)texture)->getTexture(), mipLevel);
	else if (!depth && stencil)
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_STENCIL_ATTACHMENT, type, ((const GLTexture*)texture)->getTexture(), mipLevel);
}

/*! \brief Sets which draw buffers are active
 *
 * \param (uint32_t*) attachments - Pointer to the list of attachments
 * \param (uint32_t) size - The size of the array of attachments
 */
void viv::GLFramebuffer::setDrawBuffers(uint32_t* attachments, uint32_t size) const
{
	glDrawBuffers(size, attachments);
}

/*! \brief Resizes a framebuffers renderbuffer / texture
 *
 * \param (uint32_t) width - The new width
 * \param (uint32_t) height - The new height
 */
void viv::GLFramebuffer::resize(uint32_t width, uint32_t height)
{
	Framebuffer::resize(width, height);

	if(this->depthTex == nullptr) {
		glBindRenderbuffer(GL_RENDERBUFFER, this->rbo);
		glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT24, this->rboWidth, this->rboHeight);
	}

	else {
		this->depthTex->resize(width, height, (const void *)nullptr);
	}
}

/*! \brief Gets the OpenGL framebuffer id
 *
 * \return (uint32_t) The OpenGL framebuffer id
 */
uint32_t viv::GLFramebuffer::getFramebuffer() const
{
	return this->fbo;
}

/*! \brief Gets the OpenGL renderbuffer id
 *
 * \return (uint32_t) The OpenGL renderbuffer id
 */
uint32_t viv::GLFramebuffer::getRenderbuffer() const
{
	return this->rbo;
}

/*! \brief GLFramebuffer copy assignment operator
 *
 * \param (const GLFramebuffer &) fb - The OpenGL framebuffer to copy
 *
 * \return (const GLFramebuffer &) This OpenGL framebuffer after copying
 */
const viv::GLFramebuffer & viv::GLFramebuffer::operator=(const GLFramebuffer &gfb)
{
	Framebuffer::operator=(gfb);

	glGenFramebuffers(1, &this->fbo);

	if(gfb.depthTex == nullptr) {
		glBindFramebuffer(GL_FRAMEBUFFER, this->fbo);
		glGenRenderbuffers(1, &this->rbo);
		glBindRenderbuffer(GL_RENDERBUFFER, this->rbo);
		glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT24, this->rboWidth, this->rboHeight);
		glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, this->rbo);
	}

	else {
		this->depthTex = gfb.depthTex;
		this->attachDepthStencil(this->depthTex, true, false, GL_TEXTURE_2D, 0);
	}

	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	return *this;
}

/*! \brief GLFramebuffer destructor
 */
viv::GLFramebuffer::~GLFramebuffer()
{
	if(this->depthTex == nullptr)
		glDeleteRenderbuffers(1, &this->rbo);
	else
		delete this->depthTex;
	glDeleteFramebuffers(1, &this->fbo);
}
