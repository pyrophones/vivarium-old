/*! \file indexBuffer.cpp
 * \author Giovanni Aleman
 *
 ******************************************************************************
 * Copyright (c) 2016-2019 Giovanni Aleman
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************
 */

#include "indexBuffer.h"

/*! \brief Default index buffer constructor
 */
viv::IndexBuffer::IndexBuffer() : Buffer()
{

}

/*! \brief IndexBuffer copy constructor
 *
 * \param (const IndexBuffer &) b - The index buffer to copy from
 */
viv::IndexBuffer::IndexBuffer(const IndexBuffer &vb) : Buffer(vb)
{

}

/*! \brief IndexBuffer copy assigment operator
 *
 * \param (const IndexBuffer &) b - The index buffer to copy from
 *
 * \return (const IndexBuffer &) This index buffer after copying
 */
const viv::IndexBuffer & viv::IndexBuffer::operator=(const IndexBuffer &vb)
{
	Buffer::operator=(vb);
	return *this;
}

/*! \brief Default index buffer destructor
 */
viv::IndexBuffer::~IndexBuffer()
{

}
