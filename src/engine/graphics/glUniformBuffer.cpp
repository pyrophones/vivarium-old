/*! \file glUniform.cpp
 * \author Giovanni Aleman
 *
 ******************************************************************************
 * Copyright (c) 2016-2019 Giovanni Aleman
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************
 */
#include "glUniformBuffer.h"

/*! \brief Default GLUniformBuffer constructor
 *
 * \param (const void*) data - The data for the OpenGL uniform buffer to use
 */
viv::GLUniformBuffer::GLUniformBuffer() : Buffer(), GLBuffer(), UniformBuffer()
{

}

/*! \brief GLUniformBuffer copy constructor
 *
 * \param (const GLUniformBuffer &) b - The OpenGL uniform buffer to copy from
 */
viv::GLUniformBuffer::GLUniformBuffer(const GLUniformBuffer &gub) : Buffer(gub), GLBuffer(gub), UniformBuffer(gub)
{

}

/*! \brief Binds the uniform buffer
 */
void viv::GLUniformBuffer::bind() const
{
	glBindBuffer(GL_UNIFORM_BUFFER, this->bufferId);
}

/*! \brief Unbinds the uniform buffer
 */
void viv::GLUniformBuffer::unbind() const
{
	glBindBuffer(GL_UNIFORM_BUFFER, 0);
}

/*! \brief Adds data to the uniform buffers
 */
void viv::GLUniformBuffer::bufferData(size_t size, const void* data, bool dynamic) const
{
	glBufferData(GL_UNIFORM_BUFFER, size, data, dynamic ? GL_DYNAMIC_DRAW : GL_STATIC_DRAW);
}

/*! \brief Adds Subdata to the buffer
 *
 * \param (uint32_t) offset - The offset of the data
 * \param (uint32_t) size - The size of the data
 * \param (const void*) subData - The subdata
 */
void viv::GLUniformBuffer::bufferSubData(uint32_t offset, uint32_t size, const void* subData) const
{
	glBufferSubData(GL_UNIFORM_BUFFER, offset, size, subData);
}

/*! \brief Binds the buffer to a binding point
 *
 * \param (uint32_t) loc - The binding point for the buffer
 */
void viv::GLUniformBuffer::bindBufferBase(uint32_t loc) const
{
	glBindBufferBase(GL_UNIFORM_BUFFER, loc, this->bufferId);
}

/*! \brief GLUniformBuffer copy assigment operator
 *
 * \param (const GLUniformBuffer &) b - The OpenGL uniform buffer to copy from
 *
 * \return (const GLUniformBuffer &) This OpenGL uniform buffer after copying
 */
const viv::GLUniformBuffer & viv::GLUniformBuffer::operator=(const GLUniformBuffer &gub)
{
	Buffer::operator=(gub);
	GLBuffer::operator=(gub);
	UniformBuffer::operator=(gub);

	return *this;
}

/*! \brief Default GLUniformBuffer destructor
 */
viv::GLUniformBuffer::~GLUniformBuffer()
{

}
