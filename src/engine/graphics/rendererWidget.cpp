/*! \file rendererWidget.cpp
 * \author Giovanni Aleman
 *
 ******************************************************************************
 Copyright (c) 2016-2019 Giovanni Aleman

 This Source Code Form is subject to the terms of the Mozilla Public
 License, v. 2.0. If a copy of the MPL was not distributed with this
 file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************
 */

#include "rendererWidget.h"
#include "ui_rendererWidget.h"
#include "system.h"

/*! \brief Constructor for the renderer
 *
 * \param (QWidget*) parent - The parent of the widget
 */
viv::Renderer::Renderer(QWidget* parent) : QWidget(parent)
{
	this->ui = new Ui::Renderer;
	this->ui->setupUi(this);
	this->setAttribute(Qt::WA_PaintOnScreen);
	this->setAttribute(Qt::WA_NativeWindow);
	this->setMouseTracking(true);
	WindowData::outerSize = Vec2(this->frameGeometry().width(), this->frameGeometry().height());
	WindowData::innerSize = Vec2(this->width(), this->height());
	WindowData::outerPos = Vec2(this->pos().x(), this->pos().y());
	WindowData::innerPos = Vec2(this->geometry().x(), this->geometry().y());
	WindowData::center = (WindowData::innerPos + WindowData::innerSize) / 2.0f;
}

/*! \brief Initalize some variables
 */
void viv::Renderer::init(const RenderBackend* backend)
{
	this->backend = backend;
	this->backend->setClearColor(Vec4(0.0f, 0.0f, 0.0f, 1.0f));
	this->backend->setCulling(true);
	this->backend->setCullMode(RenderBackend::CullMode::BACK);

	AssetManager::getShader("defaultShader")->setUniformInt1("gRough", 0);
	AssetManager::getShader("defaultShader")->setUniformInt1("gAlbedoMetal", 1);
	AssetManager::getShader("defaultShader")->setUniformInt1("gNormalAo", 2);
	AssetManager::getShader("dirLightShader")->setUniformInt1("gDepth", 0);
	AssetManager::getShader("dirLightShader")->setUniformInt1("gRough", 1);
	AssetManager::getShader("dirLightShader")->setUniformInt1("gAlbedoMetal", 2);
	AssetManager::getShader("dirLightShader")->setUniformInt1("gNormalAo", 3);
	AssetManager::getShader("dirLightShader")->setUniformInt1("brdfLUT", 4);
	AssetManager::getShader("dirLightShader")->setUniformInt1("irradianceMap", 5);
	AssetManager::getShader("dirLightShader")->setUniformInt1("prefilterMap", 6);
	AssetManager::getShader("pointLightShader")->setUniformInt1("gDepth", 0);
	AssetManager::getShader("pointLightShader")->setUniformInt1("gRough", 1);
	AssetManager::getShader("pointLightShader")->setUniformInt1("gAlbedoMetal", 2);
	AssetManager::getShader("pointLightShader")->setUniformInt1("gNormalAo", 3);
	AssetManager::getShader("pointLightShader")->setUniformInt1("brdfLUT", 4);
	AssetManager::getShader("pointLightShader")->setUniformInt1("irradianceMap", 5);
	AssetManager::getShader("pointLightShader")->setUniformInt1("prefilterMap", 6);
	AssetManager::getShader("spotLightShader")->setUniformInt1("gDepth", 0);
	AssetManager::getShader("spotLightShader")->setUniformInt1("gRough", 1);
	AssetManager::getShader("spotLightShader")->setUniformInt1("gAlbedoMetal", 2);
	AssetManager::getShader("spotLightShader")->setUniformInt1("gNormalAo", 3);
	AssetManager::getShader("spotLightShader")->setUniformInt1("irradianceMap", 4);
	AssetManager::getShader("finalShader")->setUniformInt1("tex", 0);
	AssetManager::getShader("pbrAmbientShader")->setUniformInt1("gDepth", 0);
	AssetManager::getShader("pbrAmbientShader")->setUniformInt1("gRough", 1);
	AssetManager::getShader("pbrAmbientShader")->setUniformInt1("gAlbedoMetal", 2);
	AssetManager::getShader("pbrAmbientShader")->setUniformInt1("gNormalAo", 3);
	AssetManager::getShader("pbrAmbientShader")->setUniformInt1("brdfLUT", 4);
	AssetManager::getShader("pbrAmbientShader")->setUniformInt1("irradianceMap", 5);
	AssetManager::getShader("pbrAmbientShader")->setUniformInt1("prefilterMap", 6);

	//NOTE: This and the other text rendering stuff will move to a different place.
	//textShader.loadShader("shaders/vertTxt.glsl",
	//                      "shaders/fragTxt.glsl");

	//defShaderNoTex.loadShader("shaders/defVert.glsl",
	//						  "shaders/defFragNoTex.glsl");

	//Create gBuffer
	this->gBuffer = this->backend->createFramebuffer(this->width(), this->height(), false);
	this->backend->bindFramebuffer(this->gBuffer);

	//Create framebuffer textures to use
	this->gRoughTexture = this->backend->createTexture(this->width(), this->height(), 1, 16, GL_FLOAT, GL_CLAMP_TO_EDGE, GL_NEAREST, GL_NEAREST, nullptr, false);
	this->gBuffer->attachTexture(this->gRoughTexture, 0, GL_TEXTURE_2D, 0);

	this->gAlbedoMetalTexture = this->backend->createTexture(this->width(), this->height(), 4, 16, GL_UNSIGNED_BYTE, GL_CLAMP_TO_EDGE, GL_NEAREST, GL_NEAREST, nullptr, false);
	this->gBuffer->attachTexture(this->gAlbedoMetalTexture, 1, GL_TEXTURE_2D, 0);

	this->gNormalAoTexture = this->backend->createTexture(this->width(), this->height(), 4, 16, GL_FLOAT, GL_CLAMP_TO_EDGE, GL_NEAREST, GL_NEAREST, nullptr, false);
	this->gBuffer->attachTexture(this->gNormalAoTexture, 2, GL_TEXTURE_2D, 0);

	uint32_t attachments[3] = { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1, GL_COLOR_ATTACHMENT2 };
	this->gBuffer->setDrawBuffers(attachments, 3);
	this->backend->bindFramebuffer(nullptr);

	//Create first ping pong buffer
	this->pingPongBuffer1 = this->backend->createFramebuffer(this->width(), this->height(), true);
	this->backend->bindFramebuffer(this->pingPongBuffer1);
	this->pingPongTexture1 = this->backend->createTexture(this->width(), this->height(), 4, 16, GL_UNSIGNED_BYTE, GL_CLAMP_TO_EDGE, GL_NEAREST, GL_NEAREST, nullptr, false);
	this->pingPongBuffer1->attachTexture(this->pingPongTexture1, 0, GL_TEXTURE_2D, 0);

	//Create second ping pong buffer
	this->pingPongBuffer2 = this->backend->createFramebuffer(this->width(), this->height(), true);
	this->backend->bindFramebuffer(this->pingPongBuffer2);
	this->pingPongTexture2 = this->backend->createTexture(this->width(), this->height(), 4, 16, GL_UNSIGNED_BYTE, GL_CLAMP_TO_EDGE, GL_NEAREST, GL_NEAREST, nullptr, false);
	this->pingPongBuffer2->attachTexture(this->pingPongTexture2, 0, GL_TEXTURE_2D, 0);
	this->backend->bindFramebuffer(nullptr);
	this->backend->bindFramebuffer(nullptr);

	//TODO: Move this from here
	BufferCollection* tmpCollection = backend->createBufferCollection(1, 1, 0);
	//Get buffer pointers
	const VertexBuffer* vbo = tmpCollection->getVertexBuffer(0);
	const IndexBuffer* ebo = tmpCollection->getIndexBuffer(0);
	//Bind the buffer collection, Bind and set the EBO
	tmpCollection->bind();
	ebo->bind();
	ebo->bufferData(3 * sizeof(uint32_t), AssetManager::getMeshData("fsQuad")->getIndices(), GL_STATIC_DRAW);
	//Bind VBO (vertex data) and set it's attributes
	vbo->bind();
	vbo->bufferData(3 * sizeof(Vertex), AssetManager::getMeshData("fsQuad")->getVertices(), GL_STATIC_DRAW);
	vbo->bufferAttribute(AssetManager::getShader("brdfShader")->getAttributes()["position"].index, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), 0, 0);
	vbo->bufferAttribute(AssetManager::getShader("brdfShader")->getAttributes()["uv"].index, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), sizeof(Vec3), 0);
	vbo->unbind();
	tmpCollection->unbind(); //Unbind the buffer collection

	Framebuffer* fb = this->backend->createFramebuffer(512, 512, true);
	this->backend->bindFramebuffer(fb);
	brdfLUT = this->backend->createTexture(512, 512, 2, 8, GL_FLOAT, GL_CLAMP_TO_EDGE, GL_LINEAR, GL_LINEAR, nullptr, false);
	fb->attachTexture(brdfLUT, 0, GL_TEXTURE_2D, 0);
	this->backend->resize(512, 512);
	this->backend->clear(RenderBackend::ClearMask::COLOR_AND_DEPTH);
	AssetManager::getShader("brdfShader")->use();
	brdfLUT->bind(0);
	tmpCollection->bind();
	backend->draw(RenderBackend::DrawType::TRIS, AssetManager::getMeshData("fsQuad").get(), 1);
	tmpCollection->unbind();
	this->backend->bindFramebuffer(nullptr);

	delete tmpCollection;
	this->backend->resize(this->geometry().width(), this->geometry().height());

	fsQuadAmbient = Drawable(AssetManager::getMeshData("fsQuad"), AssetManager::getShader("pbrAmbientShader"));
	fsQuadAmbient.setBufferCollection(backend->createBufferCollection(1, 1, 1));
	fsQuadAmbient.buffer = [&fsQuad = fsQuadAmbient](void) {
		//Get buffer pointers
		const VertexBuffer* vbo = fsQuad.getBufferCollection()->getVertexBuffer(0);
		const IndexBuffer* ebo = fsQuad.getBufferCollection()->getIndexBuffer(0);
		const UniformBuffer* ubo = fsQuad.getBufferCollection()->getUniformBuffer(0);

		//Bind the buffer collection, bind and set the EBO
		fsQuad.getBufferCollection()->bind();
		ebo->bind();
		ebo->bufferData(fsQuad.getMeshData()->getIndexCount() * sizeof(uint32_t), fsQuad.getMeshData()->getIndices(), GL_STATIC_DRAW);

		//Bind VBO1 (vertex data) and set it's attributes
		vbo->bind();
		vbo->bufferData(fsQuad.getMeshData()->getVertexCount() * sizeof(Vertex), fsQuad.getMeshData()->getVertices(), GL_STATIC_DRAW);
		vbo->bufferAttribute(fsQuad.getShader()->getAttributes()["vertex"].index, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), 0, 0);
		vbo->bufferAttribute(fsQuad.getShader()->getAttributes()["uv"].index, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), offsetof(Vertex, uv), 0);
		vbo->unbind();

		fsQuad.getBufferCollection()->unbind(); //Unbind the buffer collection

		//Bind and set UBO1 (Camera matrices)
		if(ubo != nullptr) {
			ubo->bind();
			ubo->bufferData(sizeof(Mat4) * sizeof(Vec4), nullptr, GL_DYNAMIC_DRAW);
			ubo->bindBufferBase(fsQuad.getShader()->getUniformBlocks()["CamProps"].blockBinding);
			ubo->unbind();
		}
	};

	fsQuadAmbient.preDraw = [&fsQuad = fsQuadAmbient](void) {
		fsQuad.getShader()->use();

		//Update the camera matrix buffer
		const UniformBuffer* ubo = fsQuad.getBufferCollection()->getUniformBuffer(0);
		if(ubo != nullptr) {
			ubo->bindBufferBase(fsQuad.getShader()->getUniformBlocks()["CamProps"].blockBinding);
			ubo->bind();
			ubo->bufferSubData(0, sizeof(Mat4), &Game::getActiveCam()->getProjViewInvMat());
			ubo->bufferSubData(sizeof(Mat4), sizeof(Vec3), &Game::getActiveCam()->getPos());
			ubo->unbind();
		}

		fsQuad.getBufferCollection()->bind();
	};

	fsQuadAmbient.postDraw = [&fsQuad = fsQuadAmbient](void) {
		fsQuad.getBufferCollection()->unbind();
	};

	this->fsQuadAmbient.buffer();

	this->finalProcess = new PostProcess(this->backend, AssetManager::getShader("finalShader"), -1);

	delete fb;

	//AssetManager::createPostProcess("ppSimpleBlurShader", 0);
	//AssetManager::createPostProcess("ppInvertShader", 1);
	//AssetManager::createPostProcess("ppAberrationShader", 2);
	//AssetManager::createPostProcess("ppBlackWhiteShader", 3);

	//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
}

/*! \brief Renders to the scene
 */
void viv::Renderer::render()
{
	//TODO: Make scenes and move these to the scene render method.
	this->backend->bindFramebuffer(this->gBuffer);
	this->backend->setDepthMask(true);
	this->backend->clear(RenderBackend::ClearMask::COLOR_AND_DEPTH);
	this->backend->setDepthFunction(RenderBackend::DepthFunction::LESS);
	this->backend->setDepthTest(true);
	this->backend->setBlending(false);
	this->backend->setCullMode(RenderBackend::CullMode::BACK);

	this->gRoughTexture->bind(0);
	this->gAlbedoMetalTexture->bind(1);
	this->gNormalAoTexture->bind(2);
	this->gBuffer->getDepthTexture()->bind(3);

	//NOTE: Should more of the drawing logic be here?
	for(auto i : AssetManager::getAllMeshes()) {
		if(i.second->shouldRender) {
			i.second->preDraw();
			this->backend->draw(RenderBackend::DrawType::TRIS, i.second->getMeshData(), i.second->getWorldMatCount());
			i.second->postDraw();
		}
	}

	this->backend->bindFramebuffer(this->pingPongBuffer1);
	this->backend->setDepthMask(false);
	this->backend->clear(RenderBackend::ClearMask::COLOR);
	this->backend->setDepthTest(false);
	this->backend->setBlending(true);
	this->backend->setBlendMode(RenderBackend::BlendEquation::ADD);
	this->backend->setBlendFunction(RenderBackend::BlendFuncParam::ONE, RenderBackend::BlendFuncParam::ONE);

	this->gBuffer->getDepthTexture()->bind(0);
	this->gRoughTexture->bind(1);
	this->gAlbedoMetalTexture->bind(2);
	this->gNormalAoTexture->bind(3);
	this->brdfLUT->bind(4);
	Game::skybox->getIrradianceMap()->bind(5);
	Game::skybox->getPrefilterMap()->bind(6);

	//Do some light drawing here
	for(uint32_t i = 0; i < Game::sceneDirectionalLights.size(); i++) {
		if(Game::sceneDirectionalLights[i]->shouldRender) {
			Game::sceneDirectionalLights[i]->preDraw();
			this->backend->draw(RenderBackend::DrawType::TRIS, Game::sceneDirectionalLights[i]->getMeshData(), Game::sceneDirectionalLights[i]->getWorldMatCount());
			Game::sceneDirectionalLights[i]->postDraw();
		}
	}

	this->backend->setCullMode(RenderBackend::CullMode::FRONT);
	for(uint32_t i = 0; i < Game::scenePointLights.size(); i++) {
		if(Game::scenePointLights[i]->shouldRender) {
			Game::scenePointLights[i]->preDraw();
			this->backend->draw(RenderBackend::DrawType::TRIS, Game::scenePointLights[i]->getMeshData(), Game::scenePointLights[i]->getWorldMatCount());
			Game::scenePointLights[i]->postDraw();
		}
	}

	for(uint32_t i = 0; i < Game::sceneSpotLights.size(); i++) {
		if(Game::sceneSpotLights[i]->shouldRender) {
			Game::sceneSpotLights[i]->preDraw();
			this->backend->draw(RenderBackend::DrawType::TRIS, Game::sceneSpotLights[i]->getMeshData(), Game::sceneSpotLights[i]->getWorldMatCount());
			Game::sceneSpotLights[i]->postDraw();
		}
	}

	this->backend->setCullMode(RenderBackend::CullMode::BACK);

	this->gBuffer->getDepthTexture()->bind(0);
	this->gRoughTexture->bind(1);
	this->gAlbedoMetalTexture->bind(2);
	this->gNormalAoTexture->bind(3);
	this->brdfLUT->bind(4);
	Game::skybox->getIrradianceMap()->bind(5);
	Game::skybox->getPrefilterMap()->bind(6);
	fsQuadAmbient.preDraw();
	this->backend->draw(RenderBackend::DrawType::TRIS, fsQuadAmbient.getMeshData(), 1);
	fsQuadAmbient.postDraw();

	this->backend->setDepthTest(true);
	this->backend->copyImage(this->gBuffer->getDepthTexture(), this->pingPongBuffer1, 0, 0, 0, 0, 0, 0, 0, this->width(), this->height(), 1);

	this->backend->setDepthFunction(RenderBackend::DepthFunction::LEQUAL);
	this->backend->setBlending(false);
	this->backend->setCullMode(RenderBackend::CullMode::FRONT);
	Game::skybox->preDraw();
	this->backend->draw(RenderBackend::DrawType::TRIS, Game::skybox->getMeshData(), 1);
	Game::skybox->postDraw();

	this->backend->bindFramebuffer(pingPongBuffer2);
	this->pingPong1 = false;
	this->pingPongTexture1->bind(0);
	this->backend->clear(RenderBackend::ClearMask::COLOR);

	this->backend->setDepthTest(false);
	this->backend->setCullMode(RenderBackend::CullMode::BACK);
	this->backend->setBlending(true);
	this->backend->setBlendMode(RenderBackend::BlendEquation::ADD);
	this->backend->setBlendFunction(RenderBackend::BlendFuncParam::ONE, RenderBackend::BlendFuncParam::ONE_MINUS_SRC_ALPHA);

	for(auto i : AssetManager::getAllPostProcesses()) {
		if(i->shouldRender) {
			i->preDraw();
			this->backend->draw(RenderBackend::DrawType::TRIS, i->getMeshData(), 1);
			i->postDraw();
			this->pingPong1 = !this->pingPong1;
			this->backend->bindFramebuffer((this->pingPong1 ? this->pingPongBuffer1 : this->pingPongBuffer2));
			this->backend->clear(RenderBackend::ClearMask::COLOR);
			this->pingPong1 ? this->pingPongTexture2->bind(0) : this->pingPongTexture1->bind(0);
		}
	}

	this->backend->bindFramebuffer(nullptr);
	this->backend->clear(RenderBackend::ClearMask::COLOR_AND_DEPTH);
	this->finalProcess->preDraw();
	this->backend->draw(RenderBackend::DrawType::TRIS, this->finalProcess->getMeshData(), 1);
	this->finalProcess->postDraw();

	this->backend->swapBuffers(this->winId());
}

/*! \brief Override for the paintEvent call
 *
 * \param (QPaintEvent*) event - The paint event
 */
void viv::Renderer::paintEvent(QPaintEvent* event)
{
	event->ignore();
	this->render();
}

/*! \brief Override for the resize event
 *
 * \param (QResizeEvent*) event - The resize event
 */
void viv::Renderer::resizeEvent(QResizeEvent* event)
{
	QWidget::resizeEvent(event);
	WindowData::outerSize = Vec2(this->frameGeometry().width(), this->frameGeometry().height());
	WindowData::innerSize = Vec2(this->width(), this->height());
	WindowData::outerPos = Vec2(this->pos().x(), this->pos().y());
	WindowData::innerPos = Vec2(this->geometry().x(), this->geometry().y());
	WindowData::center = (WindowData::innerSize + WindowData::innerPos) / 2.0f;

	this->backend->resize(this->width(), this->height());
	this->gBuffer->resize(this->width(), this->height());
	this->gRoughTexture->resize(this->width(), this->height(), (const void*)nullptr);
	this->gAlbedoMetalTexture->resize(this->width(), this->height(), (const void*)nullptr);
	this->gNormalAoTexture->resize(this->width(), this->height(), (const void*)nullptr);

	this->pingPongBuffer1->resize(this->width(), this->height());
	this->pingPongBuffer2->resize(this->width(), this->height());
	this->pingPongTexture1->resize(this->width(), this->height(), (const void*)nullptr);
	this->pingPongTexture2->resize(this->width(), this->height(), (const void*)nullptr);

	Game::getActiveCam()->setAspectRatio((float)this->width() / (float)this->height());
}

/*! \brief Override for the Qt paint engine
 *
 * \return (QPaintEngine*) Returns null since the paint engine is not needed
 */
QPaintEngine* viv::Renderer::paintEngine() const
{
	return nullptr;
}

/*! \brief Override for a Qt key press event. Passes the event to the input manager
 *
 * \param (QKeyEvent*) event - The key event to process
 */
void viv::Renderer::keyPressEvent(QKeyEvent* event)
{
	Input::keyPressEvent(event);
}

/*! \brief Override for a Qt key release event. Passes the event to the input manager
 *
 * \param (QKeyEvent*) event - The key event to process
 */
void viv::Renderer::keyReleaseEvent(QKeyEvent* event)
{
	Input::keyReleaseEvent(event);
}

/*! \brief Override for a Qt mouse press event. Passes the event to the input manager
 *
 * \param (QMouseEvent*) event - The mouse event to process
 */
void viv::Renderer::mousePressEvent(QMouseEvent* event)
{
	Input::mousePressEvent(event);
}

/*! \brief Override for a Qt mouse release event. Passes the event to the input manager
 *
 * \param (QMouseEvent*) event - The mouse event to process
 */
void viv::Renderer::mouseReleaseEvent(QMouseEvent* event)
{
	Input::mouseReleaseEvent(event);
}

/*! \brief Override for Qt mouse movement events. Passes the event to the input manager
 *
 * \param (QMouseEvent*) event - The mouse event to process
 */
void viv::Renderer::mouseMoveEvent(QMouseEvent* event)
{
	Input::mouseMoveEvent(event);
}

/*! \brief Override for a Qt mouse leave event. Passes the event to the input manager
 *
 * \param (QEvent*) event - The event to process
 */
void viv::Renderer::leaveEvent(QEvent* event)
{
	Input::mouseLeave();
	event->ignore();
}

/*! \brief Override for a Qt wheel event. TODO: Passes the event to the input manager
 *
 * \param (QWheelEvent*) event - The wheel event to process
 */
void viv::Renderer::wheelEvent(QWheelEvent* event)
{
	QWidget::wheelEvent(event);
}

/*! \brief Destructor for the renderer
 */
viv::Renderer::~Renderer()
{
	delete this->gRoughTexture;
	delete this->gAlbedoMetalTexture;
	delete this->gNormalAoTexture;
	delete this->brdfLUT;
	delete this->gBuffer;
	delete this->pingPongTexture1;
	delete this->pingPongTexture2;
	delete this->pingPongBuffer1;
	delete this->pingPongBuffer2;
	delete this->finalProcess;
    delete this->ui;
}
