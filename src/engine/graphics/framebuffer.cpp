/*! \file frameBuffer.cpp
 * \author Giovanni Aleman
 *
 ******************************************************************************
 * Copyright (c) 2016-2019 Giovanni Aleman
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************
 */

#include "framebuffer.h"

/*! \brief Framebuffer constructor
 *
 * \param (uint32_t) width - The width of the renderbuffer
 * \param (uint32_t) height - The height of the renderbuffer
 */
viv::Framebuffer::Framebuffer(uint32_t rboWidth, uint32_t rboHeight)
{
	this->rboWidth = rboWidth;
	this->rboHeight = rboHeight;
}

/*! \brief Framebuffer copy constructor
 *
 * \param (const Framebuffer &) fb - The framebuffer to copy
 */
viv::Framebuffer::Framebuffer(const Framebuffer &fb)
{
	this->rboWidth = fb.rboWidth;
	this->rboHeight = fb.rboHeight;
}

/*! \brief Gets the depth texture of the framebuffer
 *
 * \return (const Texture*) The depth texture of the framebuffer
 */
const viv::Texture* viv::Framebuffer::getDepthTexture() const
{
	return this->depthTex;
}

/*! \brief Sets the size of the framebuffers renderbuffer
 *
 * \param (uint32_t) width - The width of the renderbuffer
 * \param (uint32_t) height - The height of the renderbuffer
 */
void viv::Framebuffer::resize(uint32_t width, uint32_t height)
{
	this->rboWidth = width;
	this->rboHeight = height;
}

/*! \brief Framebuffer copy assignment operator
 *
 * \param (const Framebuffer &) fb - The framebuffer to copy
 *
 * \return (const Framebuffer &) This framebuffer after copying
 */
const viv::Framebuffer & viv::Framebuffer::operator=(const Framebuffer &fb)
{
	this->rboWidth = fb.rboWidth;
	this->rboHeight = fb.rboHeight;
	return *this;
}

/*! \brief Framebuffer destructor
 */
viv::Framebuffer::~Framebuffer()
{

}
