/*! \file glBuffer.cpp
 * \author Giovanni Aleman
 *
 ******************************************************************************
 Copyright (c) 2016-2019 Giovanni Aleman

 This Source Code Form is subject to the terms of the Mozilla Public
 License, v. 2.0. If a copy of the MPL was not distributed with this
 file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************
 */
#include "glBuffer.h"

/*! \brief Default GLbuffer constructor
 *
 * \param (const void*) data - The data for the OpenGL buffer to use
 */
viv::GLBuffer::GLBuffer() : Buffer()
{
	glGenBuffers(1, &this->bufferId);
}

/*! \brief GLBuffer copy constructor
 *
 * \param (const GLBuffer &) b - The OpenGL buffer to copy from
 */
viv::GLBuffer::GLBuffer(const GLBuffer &gb) : Buffer(gb)
{

}

/*! \brief Unbinds the buffer
 */
void viv::GLBuffer::unbindAllBuffers()
{
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glBindBuffer(GL_UNIFORM_BUFFER, 0);
}

/*! \brief GLBuffer copy assigment operator
 *
 * \param (const GLBuffer &) b - The OpenGL buffer to copy from
 *
 * \return (const GLBuffer &) This OpenGL buffer after copying
 */
const viv::GLBuffer & viv::GLBuffer::operator=(const GLBuffer &gb)
{
	Buffer::operator=(gb);

	glGenBuffers(1, &this->bufferId);

	return *this;
}

/*! \brief Default GLBuffer destructor
 */
viv::GLBuffer::~GLBuffer()
{
	glDeleteBuffers(1, &this->bufferId);
}
