/*! \file gmath.cpp
 * \author Giovanni Aleman
 *
 ******************************************************************************
 Copyright (c) 2016-2019 Giovanni Aleman

 This Source Code Form is subject to the terms of the Mozilla Public
 License, v. 2.0. If a copy of the MPL was not distributed with this
 file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************
 */

#include "gmath.h"

viv::Mat4 viv::IDENTITY_MAT4 = viv::Mat4();
viv::Mat3 viv::IDENTITY_MAT3 = viv::Mat3();

int32_t viv::Math::random(int32_t max)
{
	return random(0, max);
}

int32_t viv::Math::random(int32_t min, int32_t max)
{
	int32_t n = max - min + 1;
	int32_t remainder = RAND_MAX % n;
	int32_t randNum;

	do {
		randNum = std::rand();
	} while(randNum >= RAND_MAX - remainder);

	return min + randNum % n;
}

float viv::Math::randomf()
{
	int32_t num = random(100);

	return (float)num / 100.0f;
}

viv::Quat glm::axisAngle(const viv::Vec3 &axis, const float &angle)
{
	return glm::angleAxis(angle, axis);
}

viv::Quat glm::yaw(const float &f)
{
	return glm::angleAxis(f, viv::Vec3(0.0f, 1.0f, 0.0f));
}

viv::Quat glm::pitch(const float &f)
{
	return glm::angleAxis(f, viv::Vec3(1.0f, 0.0f, 0.0f));
}

viv::Quat glm::roll(const float &f)
{
	return glm::angleAxis(f, viv::Vec3(0.0f, 0.0f, 1.0f));
}

/*! \brief AngelScript default constructor binding
 *
 * \param (Vec2*) self - Pointer to the Vec2
 */
void viv::ASBindings::Vec2DefCtor(Vec2* self)
{
	new (self) Vec2();
}

/*! \brief AngelScript copy constructor binding
 *
 * \param (const Vec2 &) other - The Vec2 to copy
 * \param (Vec2*) self - Pointer to the Vec2
 */
void viv::ASBindings::Vec2CopyCtor(const Vec2 &other, Vec2* self)
{
	new (self) Vec2(other);
}

/*! \brief AngelScript init constructor binding
 *
 * \param (float) x - The x value of the vector
 * \param (float) y - The y value of the vector
 * \param (Vec2*) self - Pointer to the Vec2
 */
void viv::ASBindings::Vec2InitCtor(float x, float y, Vec2* self)
{
	new (self) Vec2(x, y);
}

/*! \brief AngelScript destructor
 *
 * \param (Vec2*) self - Pointer to the memory to deallocate
 */
void viv::ASBindings::Vec2Dtor(Vec2* self)
{
	self->~Vec2();
}

/*! \brief AngelScript default constructor binding
 *
 * \param (Vec3*) self - Pointer to the Vec3
 */
void viv::ASBindings::Vec3DefCtor(Vec3* self)
{
	new (self) Vec3();
}

/*! \brief AngelScript copy constructor binding
 *
 * \param (const Vec3 &) other - The Vec3 to copy
 * \param (Vec3*) self - Pointer to the Vec3
 */
void viv::ASBindings::Vec3CopyCtor(const Vec3 &other, Vec3* self)
{
	new (self) Vec3(other);
}

/*! \brief AngelScript init constructor binding
 *
 * \param (float) x - The x value of the vector
 * \param (float) y - The y value of the vector
 * \param (float) z - The z value of the vector
 * \param (Vec3*) self - Pointer to the Vec3
 */
void viv::ASBindings::Vec3InitCtor(float x, float y, float z, Vec3* self)
{
	new (self) Vec3(x, y, z);
}

/*! \brief AngelScript destructor
 *
 * \param (Vec3*) self - Pointer to the memory to deallocate
 */
void viv::ASBindings::Vec3Dtor(Vec3* self)
{
	self->~Vec3();
}

/*! \brief AngelScript default constructor binding
 *
 * \param (Vec4*) self - Pointer to the Vec4
 */
void viv::ASBindings::Vec4DefCtor(Vec4* self)
{
	new (self) Vec4();
}

/*! \brief AngelScript copy constructor binding
 *
 * \param (const Vec4 &) other - The Vec4 to copy
 * \param (Vec4*) self - Pointer to the Vec4
 */
void viv::ASBindings::Vec4CopyCtor(const Vec4 &other, Vec4* self)
{
	new (self) Vec4(other);
}

/*! \brief AngelScript init constructor binding
 *
 * \param (float) x - The x value of the vector
 * \param (float) y - The y value of the vector
 * \param (float) z - The z value of the vector
 * \param (float) w - The z value of the vector
 * \param (Vec4*) self - Pointer to the Vec4
 */
void viv::ASBindings::Vec4InitCtor(float x, float y, float z, float w, Vec4* self)
{
	new (self) Vec4(x, y, z, w);
}

/*! \brief AngelScript destructor
 *
 * \param (Vec4*) self - Pointer to the memory to deallocate
 */
void viv::ASBindings::Vec4Dtor(Vec4* self)
{
	self->~Vec4();
}

/*! \brief AngelScript default constructor binding
 *
 * \param (Quat*) self - Pointer to the Quat
 */
void viv::ASBindings::QuatDefCtor(Quat* self)
{
	new (self) Quat();
}

/*! \brief AngelScript copy constructor binding
 *
 * \param (const Quat &) other - The Quat to copy
 * \param (Quat*) self - Pointer to the Quat
 */
void viv::ASBindings::QuatCopyCtor(const Quat &other, Quat* self)
{
	new (self) Quat(other);
}

/*! \brief AngelScript init constructor binding
 *
 * \param (float) w - The angle of the quaternion
 * \param (float) x - The x value of the axis of the quaternion
 * \param (float) y - The y value of the axis of the quaternion
 * \param (float) z - The z value of the axis of the quaternion
 * \param (Quat*) self - Pointer to the Quat
 */
void viv::ASBindings::QuatInitCtor(float w, float x, float y, float z, Quat* self)
{
	new (self) Quat(w, x, y, z);
}

/*! \brief AngelScript destructor
 *
 * \param (Quat*) self - Pointer to the memory to deallocate
 */
void viv::ASBindings::QuatDtor(Quat* self)
{
	self->~Quat();
}


/*! \brief AngelScript default constructor binding
 *
 * \param (Mat3*) self - Pointer to the Mat3
 */
void viv::ASBindings::Mat3DefCtor(Mat3* self)
{
	new (self) Mat3();
}

/*! \brief AngelScript copy constructor binding
 *
 * \param (const Mat3&) other - The Mat3 to copy
 * \param (Mat3*) self - Pointer to the Mat3
 */
void viv::ASBindings::Mat3CopyCtor(const Mat3 &other, Mat3* self)
{
	new (self) Mat3(other);
}

/*! \brief AngelScript init constructor binding
 *
 * \param (float) x1 - The x1 value
 * \param (float) y1 - The y1 value
 * \param (float) z1 - The z1 value
 * \param (float) x2 - The x2 value
 * \param (float) y2 - The y2 value
 * \param (float) z2 - The z2 value
 * \param (float) x3 - The x3 value
 * \param (float) y3 - The y3 value
 * \param (float) z3 - The z3 value
 * \param (Mat3*) self - Pointer to the Mat3
 */
void viv::ASBindings::Mat3InitCtor(float x1, float y1, float z1,
								   float x2, float y2, float z2,
								   float x3, float y3, float z3, Mat3* self)
{
	new (self) Mat3(x1, y1, z1, x2, y2, z2, x3, y3, z3);
}

/*! \brief AngelScript destructor
 *
 * \param (Mat3*) self - Pointer to the memory to deallocate
 */
void viv::ASBindings::Mat3Dtor(Mat3* self)
{
	self->~Mat3();
}

/*! \brief AngelScript default constructor binding
 *
 * \param (Mat4*) self - Pointer to the Mat4
 */
void viv::ASBindings::Mat4DefCtor(Mat4* self)
{
	new (self) Mat4();
}

/*! \brief AngelScript copy constructor binding
 *
 * \param (const Mat4 &) other - The Mat4 to copy
 * \param (Mat4*) self - Pointer to the Mat4
 */
void viv::ASBindings::Mat4CopyCtor(const Mat4 &other, Mat4* self)
{
	new (self) Mat4(other);
}

/*! \brief AngelScript init constructor binding
 *
 * \param (float) x1 - The x1 value
 * \param (float) y1 - The y1 value
 * \param (float) z1 - The z1 value
 * \param (float) w1 - The w1 value
 * \param (float) x2 - The x2 value
 * \param (float) y2 - The y2 value
 * \param (float) z2 - The z2 value
 * \param (float) w2 - The w2 value
 * \param (float) x3 - The x3 value
 * \param (float) y3 - The y3 value
 * \param (float) z3 - The z3 value
 * \param (float) w3 - The w3 value
 * \param (float) x4 - The x4 value
 * \param (float) y4 - The y4 value
 * \param (float) z4 - The z4 value
 * \param (float) w4 - The w4 value
 * \param (Mat4*) self - Pointer to the Mat4
 */
void viv::ASBindings::Mat4InitCtor(float x1, float y1, float z1, float w1,
								   float x2, float y2, float z2, float w2,
								   float x3, float y3, float z3, float w3,
								   float x4, float y4, float z4, float w4, Mat4* self)
{
	new (self) Mat4(x1, y1, z1, w1, x2, y2, z2, w2, x3, y3, z3, w3, x4, y4, z4, w4);
}

/*! \brief AngelScript destructor
 *
 * \param (Mat4*) self - Pointer to the memory to deallocate
 */
void viv::ASBindings::Mat4Dtor(Mat4* self)
{
	self->~Mat4();
}
