/*! \file fileLoader.cpp
 * \author Giovanni Aleman
 *
 ******************************************************************************
 * Copyright (c) 2016-2019 Giovanni Aleman
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************
 */
#include "fileLoader.h"
#include <stb/stb_image.h>

namespace viv
{
	namespace FileLoader
	{
		static void ProcessAINode(aiNode* node, const aiScene* scene, std::vector<Vertex> &verts, std::vector<uint32_t> &inds, std::vector<uint32_t> &offsets, std::vector<uint32_t> &amounts);
	};
};

/*! \brief Sets basic properties of the loaders
 */
void viv::FileLoader::setLoaderProperties()
{
	stbi_set_flip_vertically_on_load(true);
}

/*! \brief Converts an assimp scene to a series of meshes
 *
 * \param (aiNode*) node - The ai node to process
 * \param (const aiScene*) scene - The ai scene to gather info from
 * \param (std::vector<MeshData> &) verts - Vector to store the vertices of the meshes
 * \param (std::vector<MeshData> &) inds - Vector to store the indices of the meshes
 * \param (std::vector<MeshData> &) offsets - Vector to store the offsets of the meshes
 * \param (std::vector<MeshData> &) amounts - Vector to store the amounts of the meshes
 */
void viv::FileLoader::ProcessAINode(aiNode* node, const aiScene* scene, std::vector<Vertex> &verts, std::vector<uint32_t> &inds, std::vector<uint32_t> &offsets, std::vector<uint32_t> &amounts)
{
	//Iterate through the submeshes
	for(uint32_t i = 0; i < node->mNumMeshes; i++)
	{
		aiMesh* mesh = scene->mMeshes[node->mMeshes[i]];

		bool uvs = false;
		bool normals = false;
		bool tanBitan = false;

		offsets.push_back(inds.size());

		uint32_t indAmount = 0;

		//Get all the indices in the submesh
		for(uint32_t j = 0; j < mesh->mNumFaces; j++)
		{
			aiFace face = mesh->mFaces[j];
			indAmount += face.mNumIndices;
			for(uint32_t k = 0; k < face.mNumIndices; k++)
				inds.push_back(face.mIndices[k] + verts.size());
		}

		amounts.push_back(indAmount);

		//Check if there are uvs, normals, and bi/tans
		if(mesh->HasTextureCoords(0))
			uvs = true;
		if(mesh->HasNormals())
			normals = true;
		if(mesh->HasTangentsAndBitangents())
			tanBitan = true;

		//Iterate though all the vertices
		for(uint32_t j = 0; j < mesh->mNumVertices; j++)
		{
			Vertex v;
			v.pos = Vec3(mesh->mVertices[j].x, mesh->mVertices[j].y, mesh->mVertices[j].z);

			if(uvs)
				v.uv = Vec2(mesh->mTextureCoords[0][j].x, mesh->mTextureCoords[0][j].y);
			else
				v.uv = Vec2(0.0f);

			if(normals)
				v.norm = Vec3(mesh->mNormals[j].x, mesh->mNormals[j].y, mesh->mNormals[j].z);
			else
				v.norm = Vec3(0.0f);

			if(tanBitan) {
				v.tan = Vec3(mesh->mTangents[j].x, mesh->mTangents[j].y, mesh->mTangents[j].z);
				v.bitan = Vec3(mesh->mBitangents[j].x, mesh->mBitangents[j].y, mesh->mBitangents[j].z);
			}

			else {
				v.tan = Vec3(0.0f);
				v.bitan = Vec3(0.0f);
			}

			verts.push_back(v);
		}
	}

	//Iterate through all the children
	for(uint32_t i = 0; i < node->mNumChildren; i++)
	{
		ProcessAINode(node->mChildren[i], scene, verts, inds, offsets, amounts);
	}
}

/*! \brief Loads a mesh as meshdata
 *
 * \param (const char*) path - Path to the mesh file
 *
 * \return (std::shared_ptr<MeshData>) The vector of the mesh data
 */
std::shared_ptr<viv::MeshData> viv::FileLoader::loadMeshData(const char* path)
{
	Assimp::Importer import;
	const aiScene* scene = import.ReadFile(path, aiProcess_FindInvalidData |
												 aiProcess_GenNormals |
												 aiProcess_GenUVCoords |
												 aiProcess_CalcTangentSpace |
												 aiProcess_Triangulate |
												 aiProcess_JoinIdenticalVertices |
												 aiProcess_SortByPType |
												 aiProcess_FixInfacingNormals |
												 aiProcess_ImproveCacheLocality);

	std::vector<Vertex> verts;
	std::vector<uint32_t> inds;
	std::vector<uint32_t> indAmounts;
	std::vector<uint32_t> indOffsets;

	std::shared_ptr<MeshData> data = std::shared_ptr<MeshData>(nullptr);

	if(scene == nullptr || scene->mFlags == AI_SCENE_FLAGS_INCOMPLETE)
		return data;

	ProcessAINode(scene->mRootNode, scene, verts, inds, indOffsets, indAmounts);

	data = std::shared_ptr<MeshData>(new MeshData());
	data->copyVerticesFromVector(verts);
	data->copyIndicesFromVector(inds);
	data->copyIndexOffsetsFromVector(indOffsets);
	data->copyIndexAmountsFromVector(indAmounts);

	return data;
}

/*! \brief Loads a texture
 *
 * \param (const char*) path - Path to the texture
 * \param (unsigned char* &) data - The variable to store the data in
 * \param (int32_t*) width - The variable to store the width of the texture
 * \param (int32_t*) height - The variable to store the height of the texture
 * \param (int32_t*) channels - The variable to store the channels of the texture
 */
void viv::FileLoader::loadTextureData(const char* path, unsigned char* &data, int32_t* width, int32_t* height, int32_t* channels)
{
	data = stbi_load(path, width, height, channels, 0);

	if(data == nullptr) {
		printf("file could not be loaded!\n");
		return;
	}
}

/*! \brief Deletes texture data
 *
 * \param (unsigned char* &) data - The data to delete
 */
void viv::FileLoader::deleteTextureData(unsigned char* &data)
{
	stbi_image_free(data);
}

/*! \brief Loads an HDR texture
 *
 * \param (const char*) path - Path to the HDR texture
 * \param (float* &) data - The variable to store the data in
 * \param (int32_t*) width - The variable to store the width of the texture
 * \param (int32_t*) height - The variable to store the height of the texture
 * \param (int32_t*) channels - The variable to store the channels of the texture
 */
void viv::FileLoader::loadTextureHDR(const char* path, float* &data, int32_t* width, int32_t* height, int32_t* channels)
{
	printf("\tLoading hdr file %s . . . ", path);

	data = stbi_loadf(path, width, height, channels, 0);

	if(data == nullptr) {
		printf("file could not be loaded!\n");
		return;
	}

	printf("successful\n");
}

/*! \brief Deletes HDR texture data
 *
 * \param (float* &) data - The data to delete
 */
void viv::FileLoader::deleteTextureHDR(float* &data)
{
	stbi_image_free(data);
}

/*! \brief Loads a file as text
 *
 * \param (const char*) path - Path to the file
 * \param (char* &) data - The variable to store the data in
 */
void viv::FileLoader::loadText(const char* path, char* &data)
{
	printf("\tLoading file %s . . . ", path);

	std::ifstream file;
	file.exceptions(std::ifstream::failbit | std::ifstream::badbit);

	file.open(path, std::ios::in);

	if(!file.is_open()) {
		printf("file failed to load!\n");
		file.clear();
		std::string err = "failed to open file: ";
		err += std::string(path);
		throw std::runtime_error(err);
		return;
	}

	file.seekg(0, std::ios::end);
	size_t size = file.tellg();
	file.seekg(0, std::ios::beg);

	data = new char[size + 1];
	std::fill(data, data + sizeof(char) * (size + 1), 0);
	file.read(data, size);
	data[size] = 0;
	file.close();

	printf("file loaded successfully\n");
}

/*! \brief Deletes text data
 *
 * \param (char* &) data - The data to delete
 */
void viv::FileLoader::deleteText(char* &data)
{
	delete [] data;
}

/*! \brief Loads a material file
 *
 * \param (const char*) path - Path to the material file
 * \param (std::string*) matName - The name of material to return
 * \param (std::map<std::string, std::pair<std::string, std::string>>*) data - The variable to store the data in
 */
void viv::FileLoader::loadMaterialData(const char* path, std::string* matName, std::map<std::string, std::pair<std::string, std::string>>* data)
{
	char* text = nullptr;
	FileLoader::loadText(path, text);
	rapidxml::xml_document<char> xml;
	xml.parse<0>(text);
	FileLoader::deleteText(text);

	std::string type;
	std::string name;
	std::string file;

	rapidxml::xml_node<char>* node = xml.first_node();

	if(strcmp(node->name(), "material") == 0) {
		if(node->first_attribute("name"))
			*matName = node->first_attribute("name")->value();
		for(node = node->first_node(); node; node = node->next_sibling()) {
			type = node->name();
			for(rapidxml::xml_attribute<char>* attr = node->first_attribute(); attr; attr = attr->next_attribute()) {
				if(strcmp(attr->name(), "assetName") == 0) {
					name = attr->value();
				}

				else if(strcmp(attr->name(), "fileLocation") == 0) {
					file = attr->value();
				}
			}
			(*data)[type] = std::make_pair(name, file);
		}
	}
}

/*! \brief Loads a material file
 *
 * \param (const char*) path - Path to the shader file
 * \param (std::string*) name - The name of shader to return
 * \param (std::map<std::string, std::pair<std::string, std::string>>*) data - The variable to store the data in
 */
void viv::FileLoader::loadShader(const char* path, std::string* name, std::map<std::string, std::string>* data)
{
	char* text = nullptr;
	FileLoader::loadText(path, text);
	rapidxml::xml_document<char> xml;
	xml.parse<0>(text);
	FileLoader::deleteText(text);

	std::string file;
	rapidxml::xml_node<char>* node = xml.first_node();

	if(strcmp(node->name(), "shader") == 0) {
		if(node->first_attribute("name"))
			*name = node->first_attribute("name")->value();
		for(node = node->first_node(); node; node = node->next_sibling()) {
			for(rapidxml::xml_attribute<char>* attr = node->first_attribute(); attr; attr = attr->next_attribute()) {
				if(strcmp(attr->name(), "fileLocation") == 0) {
					file = attr->value();
				}
			}
			(*data)[node->name()] = file;
		}
	}
}
