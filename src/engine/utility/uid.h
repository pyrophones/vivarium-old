/*! \file uid.h
 * \author Giovanni Aleman
 *
 ******************************************************************************
 * Copyright (c) 2019 Giovanni Aleman
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************
 */

#ifndef UID_H
#define UID_H

#include <string>
#include <vector>
#include <memory>
#include <new>
#include <cstdint>

namespace viv
{
	/*! \class UID
	 *  \brief Class for UID
	 */
	class UID
	{
		public:
			UID();
			UID(const UID &id);
			const UID & operator=(const UID &id);
			~UID();
			static void genUID(UID &id);

		private:
			uint32_t uId = 0;
			static uint32_t curUid;
	};
}

#endif
