/*! \file meshData.cpp
 * \author Giovanni Aleman
 *
 ******************************************************************************
 * Copyright (c) 2016-2019 Giovanni Aleman
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************
 */

#include "meshData.h"

/*! \brief MeshData default constructor
 */
viv::MeshData::MeshData()
{

}

/*! \brief MeshData constructor
 *
 * \param (std::shared_ptr<Vertex[]>) verts - The vertex data of the mesh
 * \param (std::shared_ptr<uint32_t[]>) inds - The index data of the mesh
 * \param (std::shared_ptr<uint32_t[]>) offsets - The offsets of the data of the mesh
 * \param (std::shared_ptr<uint32_t[]>) amounts - The amout of vertex data of the mesh
 * \param (uint32_t) vCount - The vertex count
 * \param (uint32_t) iCount - The index count
 */
viv::MeshData::MeshData(std::shared_ptr<Vertex[]> verts, std::shared_ptr<uint32_t[]> inds, std::shared_ptr<uint32_t[]> offsets, std::shared_ptr<uint32_t[]> amounts, uint32_t vCount, uint32_t iCount, uint32_t oCount, uint32_t aCount)
{
	this->verts = std::shared_ptr<Vertex[]>(verts);
	this->inds = std::shared_ptr<uint32_t[]>(inds);
	this->offsets = std::shared_ptr<uint32_t[]>(offsets);
	this->amounts = std::shared_ptr<uint32_t[]>(amounts);
	this->vCount = vCount;
	this->iCount = iCount;
	this->oCount = oCount;
	this->aCount = aCount;
}

/*! \brief MeshData copy constructor
 *
 * \param (const MeshData &) md - The mesh data to copy
 */
viv::MeshData::MeshData(const MeshData &md)
{
	this->verts = std::shared_ptr<Vertex[]>(md.verts);
	this->inds = std::shared_ptr<uint32_t[]>(md.inds);
	this->offsets= std::shared_ptr<uint32_t[]>(md.offsets);
	this->amounts = std::shared_ptr<uint32_t[]>(md.amounts);
	this->vCount = md.vCount;
	this->iCount = md.iCount;
	this->oCount = md.oCount;
	this->aCount = md.aCount;
}

/*! \brief Copies vertex information from a vertex array
 *
 * \param (std::vector<Vertex>) verts - The vertex array to copy
 */
void viv::MeshData::copyVerticesFromVector(std::vector<Vertex> verts)
{
	this->vCount = verts.size();
	this->verts = std::shared_ptr<Vertex[]>(new Vertex[this->vCount]);
	std::copy(verts.begin(), verts.end(), this->verts.get());
}

/*! \brief Copies index information from an index array
 *
 * \param (std::vector<uint32_t>) inds - The index array to copy
 */
void viv::MeshData::copyIndicesFromVector(std::vector<uint32_t> inds)
{
	this->iCount = inds.size();
	this->inds = std::shared_ptr<uint32_t[]>(new uint32_t[this->iCount]);
	std::copy(inds.begin(), inds.end(), this->inds.get());
}

/*! \brief Copies index offset information from an index offset array
 *
 * \param (std::vector<uint32_t>) offset - The index offset array to copy
 */
void viv::MeshData::copyIndexOffsetsFromVector(std::vector<uint32_t> offsets)
{
	this->oCount = offsets.size();
	this->offsets = std::shared_ptr<uint32_t[]>(new uint32_t[this->oCount]);
	std::copy(offsets.begin(), offsets.end(), this->offsets.get());
}

/*! \brief Copies index amount information from an index array
 *
 * \param (std::vector<uint32_t>) amounts - The index amount array to copy
 */
void viv::MeshData::copyIndexAmountsFromVector(std::vector<uint32_t> amounts)
{
	this->aCount = amounts.size();
	this->amounts = std::shared_ptr<uint32_t[]>(new uint32_t[this->aCount]);
	std::copy(amounts.begin(), amounts.end(), this->amounts.get());
}

/*! \brief Gets the vertex count
 *
 * \return (uint32_t) The vertex count
 */
uint32_t viv::MeshData::getVertexCount() const
{
	return this->vCount;
}

/*! \brief Gets the index count
 *
 * \return (uint32_t) The index count
 */
uint32_t viv::MeshData::getIndexCount() const
{
	return this->iCount;
}

/*! \brief Gets the index offset count
 *
 * \return (uint32_t) The index offset count
 */
uint32_t viv::MeshData::getIndexOffsetCount() const
{
	return this->oCount;
}

/*! \brief Gets the index amount count
 *
 * \return (uint32_t) The index amount count
 */
uint32_t viv::MeshData::getIndexAmountCount() const
{
	return this->aCount;
}

/*! \brief Gets a pointer to the vertex array
 *
 * \return (const Vertex*) Pointer to the vertex array
 */
const viv::Vertex* viv::MeshData::getVertices() const
{
	return this->verts.get();
}

/*! \brief Gets a pointer to the index array
 *
 * \return (const uint32_t*) Pointer to the index array
 */
const uint32_t* viv::MeshData::getIndices() const
{
	return this->inds.get();
}

/*! \brief Gets a pointer to the index offset array
 *
 * \return (const uint32_t*) Pointer to the index offset array
 */
const uint32_t* viv::MeshData::getIndexOffsets() const
{
	return this->offsets.get();
}

/*! \brief Gets a pointer to the index amount array
 *
 * \return (const uint32_t*) Pointer to the index amount array
 */
const uint32_t* viv::MeshData::getIndexAmounts() const
{
	return this->amounts.get();
}

/*! \brief MeshData copy constructor
 *
 * \param (const MeshData &) md - The mesh data to copy
 *
 * \return (const MeshData &) This mesh data after copying
 */
const viv::MeshData & viv::MeshData::operator=(const MeshData &md)
{
	this->verts = std::shared_ptr<Vertex[]>(md.verts);
	this->inds = std::shared_ptr<uint32_t[]>(md.inds);
	this->offsets= std::shared_ptr<uint32_t[]>(md.offsets);
	this->amounts = std::shared_ptr<uint32_t[]>(md.amounts);
	this->vCount = md.vCount;
	this->iCount = md.iCount;
	this->oCount = md.oCount;
	this->aCount = md.aCount;

	return *this;
}

/*! \brief Mesh data destructor
 */
viv::MeshData::~MeshData()
{

}
