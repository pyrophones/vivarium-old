/*! \file glIndexBuffer.h
 * \author Giovanni Aleman
 *
 ******************************************************************************
 * Copyright (c) 2016-2019 Giovanni Aleman
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************
 */
#ifndef GLINDEXBUFFER_H
#define GLINDEXBUFFER_H

#include <GL/glew.h>
#include "glBuffer.h"
#include "indexBuffer.h"

namespace viv
{
	/*! \class GLIndexBuffer
	 *  \brief An OpenGL index buffer
	 */
	class GLIndexBuffer : public GLBuffer, public IndexBuffer
	{
		public:
			GLIndexBuffer();
			GLIndexBuffer(const GLIndexBuffer &gib);
			void bind() const override;
			void unbind() const override;
			virtual void bufferData(size_t size, const void* data, bool dynamic) const override;
			virtual void bufferSubData(uint32_t offset, uint32_t size, const void* subData) const override;
			const GLIndexBuffer & operator=(const GLIndexBuffer &gib);
			virtual ~GLIndexBuffer();
	};
};

#endif


