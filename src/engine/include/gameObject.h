/*! \file gameObject.h
 * \author Giovanni Aleman
 *
 ******************************************************************************
 * Copyright (c) 2016-2019 Giovanni Aleman
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************
 */


#ifndef GAMEOBJECT_H
#define GAMEOBJECT_H

#include <string>
#include <map>
#include <vector>
#include <memory>
#include <new>
#include <cstdint>
#include <GL/glew.h>
#include "component.h"
#include "gmath.h"
//#include "rapidxml.hpp"
//#include "shader.h"

namespace viv
{
	/*! \class GameObject
	 *  \brief Basic class for game objects
	 */
	class GameObject
	{
		public:
			GameObject(std::string tag = "Game Object", const Vec3 &pos = Vec3());
			GameObject(const GameObject &go);
			GameObject* addChild(std::unique_ptr<GameObject> go);
			Component* addComponent(std::unique_ptr<Component> c);
			virtual void update();

			/*! \brief Gets a component of type T
			 *
			 * \return (class T*) Pointer to the component
			 */
			template <class T>
			T* getComponent()
			{
				for(uint32_t i = 0; i < this->components.size(); i++) {
					if(dynamic_cast<T>(this->components[i]))
						return this->components[i].get();
				}

				return nullptr;
			}

			GameObject* getParent();
			GameObject* getChildByIndex(uint32_t index);
			//GameObject* getChildByTag(std::string tag);
			uint32_t getChildCount();
			std::string getTag();
			const GameObject & operator=(const GameObject &go);
			virtual ~GameObject();

			//Static methods for as bindings
			static void DefCtor(GameObject* self);
			static void CopyCtor(const GameObject &other, GameObject* self);
			static void InitCtor(std::string tag, char* scriptPath, GameObject* self);
			static void Dtor(GameObject* self);

		protected:
			//uint32_t childCount;
			GameObject* parent = nullptr;
			//NOTE: Might change this to to a custom data structure
			//std::map<std::string, std::unique_ptr<GameObject>> children;
			std::vector<std::unique_ptr<GameObject>> children;
			std::vector<std::shared_ptr<Component>> components;
			std::string tag;
	};
}

#endif

