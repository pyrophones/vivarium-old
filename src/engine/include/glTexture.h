/*! \file glTexture.h
 * \author Giovanni Aleman
 *
 ******************************************************************************
 * Copyright (c) 2016-2019 Giovanni Aleman
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************
 */

#ifndef GLTEXTURE_H
#define GLTEXTURE_H

#include <GL/glew.h>
#include "texture.h"

namespace viv
{
	/*! \class glTexture
	 *  \brief Handles creation and usage of textures
	 */
	class GLTexture : public Texture
	{
		public:
			GLTexture(int32_t width, int32_t height, int32_t channels, uint32_t bits, uint32_t type, uint32_t clampType, uint32_t minFilter, uint32_t magFilter, const void* data, bool generateMipMaps);
			GLTexture(int32_t width, int32_t height, int32_t channels, uint32_t type, uint32_t iFormat, uint32_t format, uint32_t clampType, uint32_t minFilter, uint32_t magFilter, const void* data, bool generateMipMaps);
			GLTexture(int32_t width, int32_t height, int32_t channels, uint32_t bits, uint32_t type, uint32_t clampType, uint32_t minFilter, uint32_t magFilter, const void** data, bool generateMipMaps);
			GLTexture(const GLTexture &glt);

			virtual void genMipMaps() override;
			virtual void bind(uint32_t bindingPoint) const override;
			virtual void unbind(uint32_t bindingPoint) const override;
			virtual void resize(uint32_t width, uint32_t height, const void* data) override;
			virtual void resize(uint32_t width, uint32_t height, const void** data) override;
			virtual void destroy() override;
			uint32_t getTexture() const;
			uint32_t getTextureType() const;
			uint32_t getType() const;
			uint32_t getIFormat() const;
			uint32_t getFormat() const;
			const GLTexture & operator=(const GLTexture &glt);
			virtual ~GLTexture();

		private:
			uint32_t texture; //! The texture to load
			uint32_t textureType; //! The type of texture
			uint32_t type; //! The data type for the texture data
			uint32_t iFormat; //! The internal format of the texture
			uint32_t format; //! The format of the texture

			virtual void createTexture(const void* data);
			virtual void createCubeMap(const void** data);
			void setupTexture(uint32_t clampType, uint32_t minFilter, uint32_t magFilter);
			static void getTextureFormat(uint32_t type, int32_t channels, uint32_t bits, uint32_t* iFormat, uint32_t* format);
	};
};
#endif
