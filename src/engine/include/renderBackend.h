/*! \file renderBackend.h
 * \author Giovanni Aleman
 *
 ******************************************************************************
 * Copyright (c) 2016-2019 Giovanni Aleman
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************
 */
#ifndef RENDERBACKEND_H
#define RENDERBACKEND_H

#include <qt5/QtGui/qwindowdefs.h>
#include "bufferCollection.h"
#include "framebuffer.h"
#include "texture.h"
#include "shader.h"
#include "meshData.h"

namespace viv
{
	/*! \class RenderBackend
	 *  \brief Controls functions relating to the renderer
	 */
	class RenderBackend
	{
		public:
			enum DrawType { POINTS, LINES, TRIS, QUADS };
			enum CullMode { FRONT, BACK, FRONT_AND_BACK };
			enum ClearMask { COLOR, DEPTH, COLOR_AND_DEPTH };
			enum BlendEquation { ADD, SUB };
			enum BlendFuncParam { SRC_COLOR, DST_COLOR, ONE, ZERO, SRC_ALPHA, ONE_MINUS_SRC_ALPHA, DST_ALPHA, ONE_MINUS_DST_ALPHA };
			enum DepthFunction { LESS, LEQUAL, EQUAL, GEQUAL, GREATER };
			enum BindingType { NORMAL, READ, WRITE };

			RenderBackend();
			RenderBackend(const RenderBackend &rb);
			virtual int32_t init(WId winId) = 0;
			virtual BufferCollection* createBufferCollection(uint32_t vertexBufferAmount, uint32_t indexBufferAmount, uint32_t uniformBufferAmount) const = 0;
			virtual Framebuffer* createFramebuffer(uint32_t width, uint32_t height, bool renderbuffer) const = 0;
			virtual Texture* createTexture(int32_t width, int32_t height, int32_t channels, uint32_t bits, uint32_t type, uint32_t clampType, uint32_t minFilter, uint32_t magFilter, const void* data, bool generateMipMaps) const = 0;
			virtual Texture* createTexture(int32_t width, int32_t height, int32_t channels, uint32_t type, uint32_t iFormat, uint32_t format, uint32_t clampType, uint32_t minFilter, uint32_t magFilter, const void* data, bool generateMipMaps) const = 0;
			virtual Texture* createCubeMap(int32_t width, int32_t height, int32_t channels, uint32_t bits, uint32_t type, uint32_t clampType, uint32_t minFilter, uint32_t magFilter, const void** data, bool generateMipMaps) const = 0;
			virtual void copyImage(const Texture* t1, const Texture* t2, uint32_t srcLvl, uint32_t srcX, uint32_t srcY, uint32_t srcZ, uint32_t dstLvl, uint32_t dstX, uint32_t dstY, uint32_t dstZ, uint32_t width, uint32_t height, uint32_t depth) const = 0;
			virtual void copyImage(const Texture* t1, const Framebuffer* t2, uint32_t srcLvl, uint32_t srcX, uint32_t srcY, uint32_t srcZ, uint32_t dstX, uint32_t dstY, uint32_t dstZ, uint32_t width, uint32_t height, uint32_t depth) const = 0;
			virtual void copyImage(const Framebuffer* t1, const Texture* t2, uint32_t srcX, uint32_t srcY, uint32_t srcZ, uint32_t dstLvl, uint32_t dstX, uint32_t dstY, uint32_t dstZ, uint32_t width, uint32_t height, uint32_t depth) const = 0;
			virtual void copyImage(const Framebuffer* t1, const Framebuffer* t2, uint32_t srcX, uint32_t srcY, uint32_t srcZ, uint32_t dstX, uint32_t dstY, uint32_t dstZ, uint32_t width, uint32_t height, uint32_t depth) const = 0;
			virtual void bindFramebuffer(const Framebuffer* fb, BindingType bt = BindingType::NORMAL) const = 0;
			virtual void setCurrentShader(Shader* shader);
			virtual void setDepthMask(bool enabled) const = 0;
			virtual void setDepthTest(bool enabled) const = 0;
			virtual void setDepthFunction(DepthFunction df) const = 0;
			virtual void setBlending(bool enabled) const = 0;
			virtual void setBlendMode(BlendEquation be) const = 0;
			virtual void setBlendFunction(BlendFuncParam bfp1, BlendFuncParam bfp2) const = 0;
			virtual void setCulling(bool enabled) const = 0;
			virtual void setCullMode(CullMode cm) const = 0;
			virtual void setClearColor(const Vec4 &color) const = 0;
			virtual void clear(ClearMask cm) const = 0;
			virtual void draw(DrawType type, const MeshData* md, uint32_t instances) const = 0;
			virtual void swapBuffers(const WId &winId) const = 0;
			virtual void resize(int32_t width, int32_t height) const = 0;
			const RenderBackend & operator=(const RenderBackend &rb);
			virtual ~RenderBackend();
		private:
			Shader* curShader = nullptr;
	};
};

#endif



