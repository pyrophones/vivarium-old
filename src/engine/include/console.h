/*! \file console.h
 * \author Giovanni Aleman
 *
 ******************************************************************************
 Copyright (c) 2016-2019 Giovanni Aleman

 This Source Code Form is subject to the terms of the Mozilla Public
 License, v. 2.0. If a copy of the MPL was not distributed with this
 file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************
 */


#ifndef CONSOLE_H
#define CONSOLE_H

#include <iostream>
#include <string.h>
#include <deque>
#include <vector>
#include <string>

namespace viv
{
	/*! Structure for a command
	 */
	typedef struct Command {
		public:
			Command();
			Command(const char* text);
			void freeMem();
			const char* getMsg();

		private:
			char* msg;

	} Command;

	static std::deque<Command> commandLog; //Holds command history
	static std::vector<Command> commandList; //List of available commands
	static std::string currentInput; //Current player input
	static std::string saveInput; //Saves the players current input if another the up arrow was pressed
	static int32_t inputMarker = -1; //Used to mark the current input
	static glm::vec3 cColor(0.0, 0.0, 0.0); //The current color of the console

	void log(const char* cmd);
	void drawConsole();
	void input(uint32_t codepoint);
	void delLastChar();
	void sendInput();
	void prevInput();
	void nextInput();
	void lastChar();
};
#endif
