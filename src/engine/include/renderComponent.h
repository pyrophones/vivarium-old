/*! \file renderComponent.h
 * \author Giovanni Aleman
 *
 ******************************************************************************
 * Copyright (c) 2016-2019 Giovanni Aleman
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************
 */

#ifndef RENDERCOMPONENT_H
#define RENDERCOMPONENT_H

#include "component.h"

namespace viv
{
	/*! \class RenderComponent
	 *  \brief Base class for render components
	 */
	class RenderComponent : public Component
	{
		public:
			RenderComponent();
			RenderComponent(const RenderComponent &r);
			virtual void update() = 0;
			const RenderComponent & operator=(const RenderComponent &r);
			virtual ~RenderComponent();

			//Static methods for as bindings
			//static void DefCtor(RenderComponent* self);
			//static void CopyCtor(const RenderComponent &other, RenderComponent* self);
			//static void InitCtor(RenderComponent* self);
			//static void Dtor(RenderComponent* self);
	};
};

#endif



