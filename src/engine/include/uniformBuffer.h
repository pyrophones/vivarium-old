/*! \file uniformBuffer.h
 * \author Giovanni Aleman
 *
 ******************************************************************************
 * Copyright (c) 2016-2019 Giovanni Aleman
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************
 */

#ifndef UNIFORMBUFFER_H
#define UNIFORMBUFFER_H

#include "buffer.h"

namespace viv
{
	/*! \class UniformBuffer
	 *  \brief Holds methods specific for uniform buffers
	 */
	class UniformBuffer : public virtual Buffer
	{
		public:
			UniformBuffer();
			UniformBuffer(const UniformBuffer &ub);
			virtual void bind() const = 0;
			virtual void unbind() const = 0;
			virtual void bufferData(size_t size, const void* data, bool dynamic) const = 0;
			virtual void bufferSubData(uint32_t offset, uint32_t size, const void* subData) const = 0;
			virtual void bindBufferBase(uint32_t) const = 0;
			const UniformBuffer & operator=(const UniformBuffer &ub);
			virtual ~UniformBuffer();
	};
};

#endif


