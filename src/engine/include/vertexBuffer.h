/*! \file vertexBuffer.h
 * \author Giovanni Aleman
 *
 ******************************************************************************
 * Copyright (c) 2016-2019 Giovanni Aleman
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************
 */

#ifndef VERTEXBUFFER_H
#define VERTEXBUFFER_H

#include "buffer.h"

namespace viv
{
	/*! \class VertexBuffer
	 *  \brief Holds information specific for vertex buffers
	 */
	class VertexBuffer : public virtual Buffer
	{
		public:
			VertexBuffer();
			VertexBuffer(const VertexBuffer &vb);
			virtual void bind() const override = 0;
			virtual void unbind() const override = 0;
			virtual void bufferData(size_t size, const void* data, bool dynamic) const override = 0;
			virtual void bufferSubData(uint32_t offset, uint32_t size, const void* subData) const override= 0;
			virtual void bufferAttribute(uint32_t location, uint32_t size, uint32_t type, bool normalized, uint32_t stride, uint64_t offset, uint32_t divisor) const = 0;
			const VertexBuffer & operator=(const VertexBuffer &vb);
			virtual ~VertexBuffer();
	};
};

#endif

