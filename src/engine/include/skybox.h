/*! \file skybox.h
 * \author Giovanni Aleman
 *
 ******************************************************************************
 * Copyright (c) 2016-2019 Giovanni Aleman
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************
 */

#ifndef SKYBOX_H
#define SKYBOX_H

#include "drawable.h"
#include "texture.h"
#include "glTexture.h"

namespace viv
{
	/*! \class Skybox
	 *  \brief Skybox. Inherits from drawable
	 */
	class Skybox : public Drawable
	{
		public:
			Skybox(const RenderBackend* backend, std::shared_ptr<Shader> s);
			Skybox(const Skybox &sb);
			void setDepthTexture(const Texture* t);
			void setTextureFromEquiRect(const RenderBackend* backend, const std::shared_ptr<Texture> &tex);
			const Texture* getIrradianceMap();
			const Texture* getPrefilterMap();
			const Skybox & operator=(const Skybox &sb);
			virtual ~Skybox();

		private:
			Texture* cubeMap;
			Texture* irradianceMap;
			Texture* prefilterMap;

			virtual Texture* convertToCubeMap(const RenderBackend* backend, Framebuffer *fb, const Texture* t);
			virtual Texture* generateIrradianceMap(const RenderBackend* backend, Framebuffer* fb);
			virtual Texture* generatePrefilterMap(const RenderBackend* backend, Framebuffer* fb);
	};
};

#endif

