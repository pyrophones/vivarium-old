/*! \file shader.h
 * \author Giovanni Aleman
 *
 ******************************************************************************
 Copyright (c) 2016-2019 Giovanni Aleman

 This Source Code Form is subject to the terms of the Mozilla Public
 License, v. 2.0. If a copy of the MPL was not distributed with this
 file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************
 */


#ifndef SHADER_H
#define SHADER_H

#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <unordered_map>
#include <GL/glew.h>

namespace viv
{
	/*! \class Shader
	 *  \brief Handles creation and usage of shaders
	 */
	class Shader
	{
		public:
			enum ShaderType { VERT, TESC, TESE, GEOM, FRAG, COMP };
			struct AttributeData
			{
				uint32_t index;
				int32_t size;
				uint64_t offset;
			};

			struct BufferBlockData
			{
				int32_t size;
				int32_t blockBinding;
			};

			struct UniformData
			{
				int32_t location;
				int32_t size;
			};

			Shader(std::string name);
			Shader(const Shader &shader);
			void loadShader(const char* shaderCode, ShaderType type);
			void linkProgram();
			void updateLocations();
			void setUniformFloat1(std::string name, float data);
			void setUniformInt1(std::string name, int32_t data);
			void setUniformUint1(std::string name, uint32_t data);
			void setUniformFloatPtr(std::string name, uint32_t uniformNum, uint32_t size, const float* data);
			void setUniformIntPtr(std::string name, uint32_t uniformNum, uint32_t size, const int32_t* data);
			void setUniformUintPtr(std::string name, uint32_t uniformNum, uint32_t size, const uint32_t* data);
			std::unordered_map<std::string, AttributeData> & getAttributes();
			std::unordered_map<std::string, BufferBlockData> & getUniformBlocks();
			std::unordered_map<std::string, BufferBlockData> & getShaderStorageBlocks();
			uint32_t getProgram() const;
			std::string getName() const;
			void use();
			const Shader & operator=(const Shader &shader);
			~Shader();

		private:
			std::unordered_map<std::string, AttributeData> attributes;
			std::unordered_map<std::string, BufferBlockData> uniformBlocks;
			std::unordered_map<std::string, UniformData> uniforms;
			std::unordered_map<std::string, BufferBlockData> shaderStorageBlocks;
			std::string name;

			uint32_t program;
			int32_t attributeCount = 0;
			int32_t uniformBlockCount = 0;
			int32_t uniformCount = 0;
			int32_t shaderStorageBlockCount = 0;

			static uint32_t bindPoint;
	};
};
#endif
