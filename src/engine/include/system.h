/*! \file system.h
 * \author Giovanni Aleman
 *
 ******************************************************************************
 Copyright (c) 2016-2019 Giovanni Aleman

 This Source Code Form is subject to the terms of the Mozilla Public
 License, v. 2.0. If a copy of the MPL was not distributed with this
 file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************
 */

#ifndef SYSTEM_H
#define SYSTEM_H

#include <iostream>
#include <memory>
#include <string>
#include <cstdint>
#include <QApplication>
#include <QMainWindow>
#include <ft2build.h>
#include FT_FREETYPE_H
#include <angelscript.h>
#include "game.h"
#include "gmath.h"
#include "rendererWidget.h"
#include "renderBackend.h"
#include "glRenderBackend.h"
//#include "vkRenderBackend.h"

BEGIN_AS_NAMESPACE

namespace viv
{
	/*! \class System
	 *  \brief Handles the creation and managed of the overall engine
	 */
	class System
	{
		public:
			static bool shouldClose; //! If the window should close
			System(const System&) = delete;
			const System& operator=(const System&) = delete;

			static int32_t init(QMainWindow* mainWindow = nullptr);
			static int32_t run();
			static int32_t end();

			static System* getInstance();
			static asIScriptEngine* getEngine();
			~System();

		private:
			enum RendererCode { OGL, VKN, DX11 };
			System();
			static void closeEvent();
			static void registerASClasses();
			static void asMessageCallback(const asSMessageInfo* msg,  void* param);
			static void asPrint(const std::string &str);

			static std::unique_ptr<System> instance; //! The instance of the system singleton
			static RendererCode rendererCode; //! The code for the renderer;
			static RenderBackend* renderBackend; //! The abstract renderer
			static Renderer* renderer; //! The renderer

			//Angelscript variable
			static asIScriptEngine* asEngine; //! The Angelscript engine
			//FreeType variables
			static FT_Library lib; //! The FreeType library
			static FT_Face face; //! The FreeType font face
	};
};

#endif
