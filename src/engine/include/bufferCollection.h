/*! \file bufferCollection.h
 * \author Giovanni Aleman
 *
 ******************************************************************************
 * Copyright (c) 2016-2019 Giovanni Aleman
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************
 */
#ifndef BUFFERCOLLECTION_H
#define BUFFERCOLLECTION_H

#include <vector>
#include <GL/glew.h>
#include "vertexBuffer.h"
#include "indexBuffer.h"
#include "uniformBuffer.h"

namespace viv
{
	/*! \class BufferCollection
	 *  \brief An collection of buffers
	 */
	class BufferCollection
	{
		public:
			enum BufferType { VERTEX, INDEX, UNIFORM };

			BufferCollection();
			BufferCollection(const BufferCollection &bc);
			virtual void bind() const = 0;
			virtual void unbind() const = 0;
			virtual const VertexBuffer* getVertexBuffer(uint32_t index) const = 0;
			virtual const IndexBuffer* getIndexBuffer(uint32_t index) const = 0;
			virtual const UniformBuffer* getUniformBuffer(uint32_t index) const = 0;
			virtual void deleteBuffer(BufferCollection::BufferType type, uint32_t index) = 0;
			const BufferCollection & operator=(const BufferCollection &bc);
			virtual ~BufferCollection();

		private:
			virtual void createBuffer(BufferCollection::BufferType type, uint32_t amount) = 0;
	};
};

#endif


