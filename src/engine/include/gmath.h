/*! \file gmath.h
 * \author Giovanni Aleman
 *
 ******************************************************************************
 Copyright (c) 2016-2019 Giovanni Aleman

 This Source Code Form is subject to the terms of the Mozilla Public
 License, v. 2.0. If a copy of the MPL was not distributed with this
 file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************
 */

#ifndef GMATH_H
#define GMATH_H

//Includes
#include <new>
#include <cmath>
#include <cstdint>
#define GLM_FORCE_SSE3
#include <glm/glm.hpp>
#include <glm/gtx/quaternion.hpp>
#include <glm/gtx/euler_angles.hpp>
#include <glm/gtc/matrix_transform.hpp>

namespace viv
{
	/*! \namespace Math
	 *  \brief Namespace for math functions
	 */
	namespace Math
	{
		const float TO_RAD = M_PI / 180.0f; //! Constant for converting to radians
		const float TO_DEG = 180.0f / M_PI; //! Constant for converting to degrees
		int32_t random(int32_t max);
		int32_t random(int32_t min, int32_t max);
		float randomf();
	};

	typedef glm::vec2 Vec2; //! Define Vec2 as a glm::vec2
	typedef glm::vec3 Vec3; //! Define Vec3 as a glm::vec3
	typedef glm::vec4 Vec4; //! Define Vec4 as a glm::vec4
	typedef glm::quat Quat; //! Define Quat as a glm::quat
	typedef glm::mat3 Mat3; //! Define Mat3 as a glm::mat3
	typedef glm::mat4 Mat4; //! Define Mat4 as a glm::mat4

	extern Mat4 IDENTITY_MAT4;
	extern Mat3 IDENTITY_MAT3;

	/*! \namespace ASBindings
	 *  \brief contains static methods for AS bindings
	 */
	namespace ASBindings
	{
		void Vec2DefCtor(Vec2* self);
		void Vec2CopyCtor(const Vec2 &other, Vec2* self);
		void Vec2InitCtor(float x, float y, Vec2* self);
		void Vec2Dtor(Vec2* self);

		void Vec3DefCtor(Vec3* self);
		void Vec3CopyCtor(const Vec3 &other, Vec3* self);
		void Vec3InitCtor(float x, float y, float z, Vec3* self);
		void Vec3Dtor(Vec3* self);

		void Vec4DefCtor(Vec4* self);
		void Vec4CopyCtor(const Vec4 &other, Vec4* self);
		void Vec4InitCtor(float x, float y, float z, float w, Vec4* self);
		void Vec4Dtor(Vec4* self);

		void QuatDefCtor(Quat* self);
		void QuatCopyCtor(const Quat &other, Quat* self);
		void QuatInitCtor(float w, float x, float y, float z, Quat* self);
		void QuatDtor(Quat* self);

		void Mat3DefCtor(Mat3* self);
		void Mat3CopyCtor(const Mat3 &other, Mat3* self);
		void Mat3InitCtor(float x1, float y1, float z1,
						  float x2, float y2, float z2,
						  float x3, float y3, float z3, Mat3* self);
		void Mat3Dtor(Mat3* self);

		void Mat4DefCtor(Mat4* self);
		void Mat4CopyCtor(const Mat4 &other, Mat4* self);
		void Mat4InitCtor(float x1, float y1, float z1, float w1,
						  float x2, float y2, float z2, float w2,
						  float x3, float y3, float z3, float w3,
						  float x4, float y4, float z4, float w4, Mat4* self);
		void Mat4Dtor(Mat4* self);
	};
};

/*! \namespace glm
 *  \brief Some basic extentions to glm
 */
namespace glm
{
	viv::Quat axisAngle(const viv::Vec3 &, const float &angle);
	viv::Quat yaw(const float &f);
	viv::Quat pitch(const float &f);
	viv::Quat roll(const float &f);
};

#endif
