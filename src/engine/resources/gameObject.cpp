/*! \file gameObject.cpp
 * \author Giovanni Aleman
 *
 ******************************************************************************
 * Copyright (c) 2016-2019 Giovanni Aleman
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************
 */

#include "gameObject.h"

viv::GameObject::GameObject(std::string tag, const Vec3 &pos)
{
	this->tag = tag;
	this->addComponent(std::shared_ptr<Transform>(new Transform(pos)));
}

viv::GameObject::GameObject(const GameObject &go)
{
	this->tag = go.tag;

	for(uint32_t i = 0; i < go.children.size(); i++) {
		this->addChild(std::shared_ptr<decltype(go.children[i])>(new go.children[i]));
	}
}

viv::GameObject* viv::GameObject::addChild(std::unique_ptr<GameObject> go)
{
	this->children.push_back(std::move(go));

	return this->children[this->children.size() - 1].get();
}

viv::Component* viv::GameObject::addComponent(std::unique_ptr<Component> c)
{
	this->components.push_back(std::move(c));

	return this->components[this->components.size() - 1].get();
}

void viv::GameObject::update()
{
	for(uint32_t i = 0; i < this->components.size(); i++)
		this->components[i]->update();
}

viv::GameObject* viv::GameObject::getParent()
{
	return this->parent;
}

viv::GameObject* viv::GameObject::getChildByIndex(uint32_t index)
{
	return this->children[index].get();
}

//viv::GameObject* viv::GameObject::getChildByTag(std::string tag)
//{

//}

uint32_t viv::GameObject::getChildCount()
{
	return this->children.size();
}

std::string viv::GameObject::getTag()
{
	return this->tag;
}

const viv::GameObject & viv::GameObject::operator=(const GameObject &go)
{

}

viv::GameObject::~GameObject()
{

}

void viv::GameObject::DefCtor(GameObject* self)
{

}

void viv::GameObject::CopyCtor(const GameObject &other, GameObject* self)
{

}

void viv::GameObject::InitCtor(std::string tag, char* scriptPath, GameObject* self)
{

}

void viv::GameObject::Dtor(GameObject* self)
{

}
