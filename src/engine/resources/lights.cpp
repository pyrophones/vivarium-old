/*! \file lights.cpp
 * \author Giovanni Aleman
 *
 ******************************************************************************
 Copyright (c) 2016-2019 Giovanni Aleman

 This Source Code Form is subject to the terms of the Mozilla Public
 License, v. 2.0. If a copy of the MPL was not distributed with this
 file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************
 */

#include "lights.h"
#include "game.h"

/*! \brief Directional Light constructor
 *
 * \param (const RenderBackend*) backend - The rendering backend to use
 * \param (const Light &) l - The light data to use
 * \param (std::shared_ptr<Shader>) s - The shader for the light to use
 */
viv::DirectionalLight::DirectionalLight(const RenderBackend* backend, std::shared_ptr<Shader> s, const Light &l)
					  : Drawable(AssetManager::getMeshData("fsQuad"), s)
{
	this->l = l;
	this->bufferCollection = backend->createBufferCollection(1, 1, 1);

	this->buffer = [this](void) {
		 	//Get buffer pointers
		const VertexBuffer* vbo = this->bufferCollection->getVertexBuffer(0);
		const IndexBuffer* ebo = this->bufferCollection->getIndexBuffer(0);
		const UniformBuffer* ubo = this->bufferCollection->getUniformBuffer(0);

		//Bind the buffer colelction, Bind and set the EBO
		this->bufferCollection->bind();
		ebo->bind();
		ebo->bufferData(this->md->getIndexCount() * sizeof(uint32_t), this->md->getIndices(), GL_STATIC_DRAW);

		//Bind VBO1 (vertex data) and set it's attributes
		vbo->bind();
		vbo->bufferData(this->md->getVertexCount() * sizeof(Vertex), this->md->getVertices(), GL_STATIC_DRAW);
		vbo->bufferAttribute(this->shad->getAttributes()["position"].index, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), 0, 0);
		vbo->unbind();

		this->bufferCollection->unbind(); //Unbind the buffer collection

		//Bind and set UBO2 (Lighting Info)
		if(ubo != nullptr) {
			ubo->bind();
			ubo->bufferData(sizeof(Light) + 2 * sizeof(Vec4) + sizeof(Mat4), nullptr, GL_DYNAMIC_DRAW);
			ubo->bindBufferBase(this->shad->getUniformBlocks()["LightingInfo"].blockBinding);
			ubo->unbind();
		}
	};

	this->preDraw = [this](void) {
		this->shad->use();

		//TODO: Move texture stuff here
		Mat4 mats[this->worldMats.size()];

		for(uint32_t i = 0; i < this->worldMats.size(); i++)
			mats[i] = *this->worldMats[i];

		const UniformBuffer* ubo = this->bufferCollection->getUniformBuffer(0);
		if(ubo != nullptr) {
			ubo->bindBufferBase(this->shad->getUniformBlocks()["LightingInfo"].blockBinding);
			ubo->bind();
			ubo->bufferSubData(0, sizeof(Light), &this->l);
			ubo->bufferSubData(sizeof(Light), sizeof(Vec2), &WindowData::innerSize);
			ubo->bufferSubData(sizeof(Light) + sizeof(Vec4), sizeof(Vec3), &Game::getActiveCam()->getPos());
			ubo->bufferSubData(sizeof(Light) + 2 * sizeof(Vec4), sizeof(Mat4), &Game::getActiveCam()->getProjViewInvMat());
			ubo->unbind();
		}

		this->bufferCollection->bind();
	};

	this->postDraw = [this](void) {
		this->bufferCollection->unbind();
	};

	this->buffer();
}

/*! \brief Directional Light copy constructor
 *
 * \param (const DirectionalLight &) dl - The Light to copy
 */
viv::DirectionalLight::DirectionalLight(const DirectionalLight &dl) : Drawable(dl)
{
	this->l = dl.l;
}

/*! \brief Directional Light copy constructor
 *
 * \param (const DirectionalLight &) dl - The Light to copy
 *
 * \return (const DirectionalLight &) This directional light after being assigned
 */
const viv::DirectionalLight & viv::DirectionalLight::operator=(const DirectionalLight &dl)
{
	Drawable::operator=(dl);
	this->l = dl.l;

	return *this;
}

/*! \brief Directional light destructor
 */
viv::DirectionalLight::~DirectionalLight()
{

}

//TODO: Comment these
//Static methods for as bindings
//void viv::Light::DefCtor(Light* self)
//{
//	new (self) Light();
//}
//
//void viv::Light::CopyCtor(const Light &other, Light* self)
//{
//	new (self) Light(other);
//}
//
//void viv::Light::InitCtor(const Vec3 &pos, const Vec3 &ambColor, const Vec3 &diffColor, const Vec3 &specColor,
//									 float ambIntensity, float diffIntensity, float specIntensity,
//									 float constAtten, float linearAtten, float quadAtten,
//									 const std::string tag, char* scriptPath, Light* self)
//{
//	new (self) Light(pos, ambColor, diffColor, specColor, ambIntensity, diffIntensity, specIntensity, constAtten,
//								linearAtten, quadAtten, tag, scriptPath);
//}
//
//void viv::Light::Dtor(Light* self)
//{
//	self->~Light();
//}

/*! \brief Point Light constructor
 *
 * \param (const RenderBackend*) backend - The rendering backend to use
 * \param (const Light &) l - The light data to use
 * \param (std::shared_ptr<Shader>) s - The shader for the light to use
 */
viv::PointLight::PointLight(const RenderBackend* backend, std::shared_ptr<Shader> s, const Light &l) : Drawable(AssetManager::getMeshData("sphere"), s)
{
	this->l = l;
	this->bufferCollection = backend->createBufferCollection(2, 1, 2);

	this->buffer = [this](void) {
		//Get buffer pointers
		const VertexBuffer* vbo1 = this->bufferCollection->getVertexBuffer(0);
		const VertexBuffer* vbo2 = this->bufferCollection->getVertexBuffer(1);
		const IndexBuffer* ebo = this->bufferCollection->getIndexBuffer(0);
		const UniformBuffer* ubo1 = this->bufferCollection->getUniformBuffer(0);
		const UniformBuffer* ubo2 = this->bufferCollection->getUniformBuffer(1);

		//Bind the buffer colelction, Bind and set the EBO
		this->bufferCollection->bind();
		ebo->bind();
		ebo->bufferData(this->md->getIndexCount() * sizeof(uint32_t), this->md->getIndices(), GL_STATIC_DRAW);

		//Bind VBO1 (vertex data) and set it's attributes
		vbo1->bind();
		vbo1->bufferData(this->md->getVertexCount() * sizeof(Vertex), this->md->getVertices(), GL_STATIC_DRAW);
		vbo1->bufferAttribute(this->shad->getAttributes()["position"].index, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), 0, 0);
		vbo1->unbind();

		if(this->shad->getAttributes().find("worldMat") != this->shad->getAttributes().end() && vbo2 != nullptr) {
			int32_t worldLoc = this->shad->getAttributes()["worldMat"].index;

			//Bind VBO3 (Matrix instance data) and set it's attributes
			vbo2->bind();
			vbo2->bufferData(sizeof(Mat4), nullptr, GL_DYNAMIC_DRAW);
			for(uint32_t i = 0; i < 4; i++) {
				vbo2->bufferAttribute(worldLoc + i, 4, GL_FLOAT, GL_FALSE, sizeof(Mat4), i * sizeof(Vec4), 1);
			}
			vbo2->unbind();
		}
		this->bufferCollection->unbind(); //Unbind the buffer collection

		//Bind and set UBO1 (Camera matrices)
		if(ubo1 != nullptr) {
			ubo1->bind();
			ubo1->bufferData(2 * sizeof(Mat4), nullptr, GL_DYNAMIC_DRAW);
			ubo1->bindBufferBase(this->shad->getUniformBlocks()["CamMats"].blockBinding);
			ubo1->unbind();
		}

		//Bind and set UBO2 (Lighting Info)
		if(ubo2 != nullptr) {
			ubo2->bind();
			ubo2->bufferData(sizeof(Light) + 2 * sizeof(Vec4) + sizeof(Mat4), nullptr, GL_DYNAMIC_DRAW);
			ubo2->bindBufferBase(this->shad->getUniformBlocks()["LightingInfo"].blockBinding);
			ubo2->unbind();
		}
	};

	this->preDraw = [this](void) {
		this->shad->use();

		//TODO: Move texture stuff here?
		Mat4 mats[this->worldMats.size()];

		for(uint32_t i = 0; i < this->worldMats.size(); i++)
			mats[i] = *this->worldMats[i];

		//UBO1 (Cam mats)
		const UniformBuffer* ubo1 = this->bufferCollection->getUniformBuffer(0);
		if(ubo1 != nullptr) {
			ubo1->bindBufferBase(this->shad->getUniformBlocks()["CamMats"].blockBinding);
			ubo1->bind();
			ubo1->bufferSubData(0, sizeof(Mat4), &Game::getActiveCam()->getViewMat());
			ubo1->bufferSubData(sizeof(Mat4), sizeof(Mat4), &Game::getActiveCam()->getProjMat());
			ubo1->unbind();
		}

		//UBO2 (Lighting Info)
		const UniformBuffer* ubo2 = this->bufferCollection->getUniformBuffer(1);
		if(ubo2 != nullptr) {
			ubo2->bindBufferBase(this->shad->getUniformBlocks()["LightingInfo"].blockBinding);
			ubo2->bind();
			ubo2->bufferSubData(0, sizeof(Light), &this->l);
			ubo2->bufferSubData(sizeof(Light), sizeof(Vec2), &WindowData::innerSize);
			ubo2->bufferSubData(sizeof(Light) + sizeof(Vec4), sizeof(Vec3), &Game::getActiveCam()->getPos());
			ubo2->bufferSubData(sizeof(Light) + 2 * sizeof(Vec4), sizeof(Mat4), &Game::getActiveCam()->getProjViewInvMat());
			ubo2->unbind();
		}

		//Bind and update the world matrix buffer and draw
		const VertexBuffer* vbo2 = this->bufferCollection->getVertexBuffer(1);
		this->bufferCollection->bind();
		if(vbo2 != nullptr) {
			vbo2->bind();
			vbo2->bufferData(this->worldMats.size() * sizeof(Mat4), (const void*)mats, GL_DYNAMIC_DRAW);
			vbo2->unbind();
		}

		this->bufferCollection->bind();
	};

	this->postDraw = [this](void) {
		this->bufferCollection->unbind();
	};

	this->buffer();
}

/*! \brief Point Light copy constructor
 *
 * \param (const PointLight &) dl - The Light to copy
 */
viv::PointLight::PointLight(const PointLight &pl) : Drawable(pl)
{
	this->l = pl.l;
}

/*! \brief Point light copy constructor
 *
 * \param (const PointLight &) pl - The light to copy
 *
 * \return (const PointLight &) This point light after being assigned
 */
const viv::PointLight & viv::PointLight::operator=(const PointLight &pl)
{
	Drawable::operator=(pl);
	this->l = pl.l;

	return *this;
}

/*! \brief Point light destructor
 */
viv::PointLight::~PointLight()
{

}

/*! \brief Spot Light constructor
 *
 * \param (const RenderBackend*) backend - The rendering backend to use
 * \param (const Light &) l - The light data to use
 * \param (std::shared_ptr<Shader>) s - The shader for the light to use
 */
viv::SpotLight::SpotLight(const RenderBackend* backend, std::shared_ptr<Shader> s, const Light &l, float range) : Drawable(AssetManager::getMeshData("cone"), s)
{
	this->l = l;
	this->range = range;
	this->bufferCollection = backend->createBufferCollection(2, 1, 2);

	this->buffer = [this](void) {
		//Get buffer pointers
		const VertexBuffer* vbo1 = this->bufferCollection->getVertexBuffer(0);
		const VertexBuffer* vbo2 = this->bufferCollection->getVertexBuffer(1);
		const IndexBuffer* ebo = this->bufferCollection->getIndexBuffer(0);
		const UniformBuffer* ubo1 = this->bufferCollection->getUniformBuffer(0);
		const UniformBuffer* ubo2 = this->bufferCollection->getUniformBuffer(1);

		//Bind the buffer colelction, Bind and set the EBO
		this->bufferCollection->bind();
		ebo->bind();
		ebo->bufferData(this->md->getIndexCount() * sizeof(uint32_t), this->md->getIndices(), GL_STATIC_DRAW);

		//Bind VBO1 (vertex data) and set it's attributes
		vbo1->bind();
		vbo1->bufferData(this->md->getVertexCount() * sizeof(Vertex), this->md->getVertices(), GL_STATIC_DRAW);
		vbo1->bufferAttribute(this->shad->getAttributes()["position"].index, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), 0, 0);
		vbo1->unbind();

		//Bind VBO2 (Instance data) and set it's attributes
		if(this->shad->getAttributes().find("worldMat") != this->shad->getAttributes().end() && vbo2 != nullptr) {
			int32_t worldLoc = this->shad->getAttributes()["worldMat"].index;

			//Bind VBO3 (Matrix instance data) and set it's attributes
			vbo2->bind();
			vbo2->bufferData(sizeof(Mat4), nullptr, GL_DYNAMIC_DRAW);
			for(uint32_t i = 0; i < 4; i++) {
				vbo2->bufferAttribute(worldLoc + i, 4, GL_FLOAT, GL_FALSE, sizeof(Mat4), i * sizeof(Vec4), 1);
			}
			vbo2->unbind();
		}
		this->bufferCollection->unbind(); //Unbind the buffer collection

		//Bind and set UBO1 (Camera matrices)
		if(ubo1 != nullptr) {
			ubo1->bind();
			ubo1->bufferData(2 * sizeof(Mat4), nullptr, GL_DYNAMIC_DRAW);
			ubo1->bindBufferBase(this->shad->getUniformBlocks()["CamMats"].blockBinding);
			ubo1->unbind();
		}

		//Bind and set UBO2 (Lighting Info)
		if(ubo2 != nullptr) {
			ubo2->bind();
			ubo2->bufferData(sizeof(Light) + 2 * sizeof(Vec4) + sizeof(Mat4), nullptr, GL_DYNAMIC_DRAW);
			ubo2->bindBufferBase(this->shad->getUniformBlocks()["LightingInfo"].blockBinding);
			ubo2->unbind();
		}
	};

	this->preDraw = [this](void) {
		this->shad->use();

		//TODO: Move texture stuff here
		Mat4 mats[this->worldMats.size()];

		for(uint32_t i = 0; i < this->worldMats.size(); i++)
			mats[i] = *this->worldMats[i];

		//UBO1 (Cam mats)
		const UniformBuffer* ubo1 = this->bufferCollection->getUniformBuffer(0);
		if(ubo1 != nullptr) {
			ubo1->bindBufferBase(this->shad->getUniformBlocks()["CamMats"].blockBinding);
			ubo1->bind();
			ubo1->bufferSubData(0, sizeof(Mat4), &Game::getActiveCam()->getViewMat());
			ubo1->bufferSubData(sizeof(Mat4), sizeof(Mat4), &Game::getActiveCam()->getProjMat());
			ubo1->unbind();
		}

		//UBO2 (Lighting Info)
		const UniformBuffer* ubo2 = this->bufferCollection->getUniformBuffer(1);
		if(ubo2 != nullptr) {
			ubo2->bindBufferBase(this->shad->getUniformBlocks()["LightingInfo"].blockBinding);
			ubo2->bind();
			ubo2->bufferSubData(0, sizeof(Light), &this->l);
			ubo2->bufferSubData(sizeof(Light), sizeof(Vec2), &WindowData::innerSize);
			ubo2->bufferSubData(sizeof(Light) + sizeof(Vec4), sizeof(Vec3), &Game::getActiveCam()->getPos());
			ubo2->bufferSubData(sizeof(Light) + 2 * sizeof(Vec4), sizeof(Mat4), &Game::getActiveCam()->getProjViewInvMat());
			ubo2->unbind();
		}

		//Bind and update the world matrix buffer and draw
		const VertexBuffer* vbo2 = this->bufferCollection->getVertexBuffer(1);
		this->bufferCollection->bind();
		if(vbo2 != nullptr) {
			vbo2->bind();
			vbo2->bufferData(this->worldMats.size() * sizeof(Mat4), (const void*)mats, GL_DYNAMIC_DRAW);
			vbo2->unbind();
		}

		this->bufferCollection->bind();
	};

	this->postDraw = [this](void) {
		this->bufferCollection->unbind();
	};

	this->buffer();
}

/*! \brief Point Light copy constructor
 *
 * \param (const PointLight &) dl - The Light to copy
 */
viv::SpotLight::SpotLight(const SpotLight &sl) : Drawable(sl)
{
	this->l = sl.l;
}

/*! \brief Spot Light copy constructor
 *
 * \param (const SpotLight &) sl - The light to copy
 *
 * \return (const SpotLight &) This spot light after being assigned
 */
const viv::SpotLight & viv::SpotLight::operator=(const SpotLight &sl)
{
	Drawable::operator=(sl);
	this->l = sl.l;

	return *this;
}

/*! \brief Spot light destructor
 */
viv::SpotLight::~SpotLight()
{

}
