/*! \file skybox.cpp
 * \author Giovanni Aleman
 *
 ******************************************************************************
 * Copyright (c) 2016-2019 Giovanni Aleman
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************
 */

#include "skybox.h"
#include "game.h"

/*! \brief Skybox Constructor
 *
 * \param (const RenderBacked*) backend - The rendering backend to use
 * \param (std::shared_ptr<Shader>) s - Pointer to the shader to use
 */
viv::Skybox::Skybox(const RenderBackend* backend, std::shared_ptr<Shader> s) : Drawable(AssetManager::getMeshData("cube"), s)
{
	this->bufferCollection = backend->createBufferCollection(1, 1, 1);

	this->buffer = [this](void) {
		//Get buffer pointers
		const VertexBuffer* vbo = this->bufferCollection->getVertexBuffer(0);
		const IndexBuffer* ebo = this->bufferCollection->getIndexBuffer(0);
		const UniformBuffer* ubo = this->bufferCollection->getUniformBuffer(0);

		//Bind the buffer collection, Bind and set the EBO
		this->bufferCollection->bind();
		ebo->bind();
		ebo->bufferData(this->md->getIndexCount() * sizeof(uint32_t), this->md->getIndices(), GL_STATIC_DRAW);

		//Bind VBO (vertex data) and set it's attributes
		vbo->bind();
		vbo->bufferData(this->md->getVertexCount() * sizeof(Vertex), this->md->getVertices(), GL_STATIC_DRAW);
		vbo->bufferAttribute(this->shad->getAttributes()["position"].index, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), 0, 0);
		vbo->unbind();

		this->bufferCollection->unbind(); //Unbind the buffer collection

		//Bind and set UBO1 (Camera matrices)
		if(ubo != nullptr) {
			ubo->bind();
			ubo->bufferData(2 * sizeof(Mat4), nullptr, GL_DYNAMIC_DRAW);
			ubo->bindBufferBase(this->shad->getUniformBlocks()["CamMats"].blockBinding);
			ubo->unbind();
		}
	};

	this->preDraw = [this](void) {
		this->shad->use();

		if(this->cubeMap != nullptr) {
			this->cubeMap->bind(0);
			this->shad->setUniformInt1("cubeMap", 0);
		}

		//Mat4 mats[this->worldMats.size()];

		//for(uint32_t i = 0; i < this->worldMats.size(); i++)
		//	mats[i] = *this->worldMats[i];

		//Update the camera matrix buffer
		const UniformBuffer* ubo = this->bufferCollection->getUniformBuffer(0);
		if(ubo != nullptr) {
			ubo->bindBufferBase(this->shad->getUniformBlocks()["CamMats"].blockBinding);
			ubo->bind();
			ubo->bufferSubData(0, sizeof(Mat4), &Game::getActiveCam()->getViewMat());
			ubo->bufferSubData(sizeof(Mat4), sizeof(Mat4), &Game::getActiveCam()->getProjMat());
			ubo->unbind();
		}

		this->bufferCollection->bind();
	};

	this->postDraw = [this](void) {
		this->bufferCollection->unbind();
		this->cubeMap->unbind(0);
	};

	this->buffer();
}

/*! \brief Skybox copy constructor
 *
 * \param (const Skybox &) sb - The skybox to copy
 */
viv::Skybox::Skybox(const Skybox &sb) : Drawable(sb)
{
	this->cubeMap = sb.cubeMap;
	this->irradianceMap = sb.irradianceMap;
	this->prefilterMap = sb.prefilterMap;
}

/*! \brief Helper function that converts an equirectangular texture to a cubemap
 *
 * \param (const RenderBacked*) backend - The rendering backend to use
 * \param (Framebuffer*) fb - The framebuffer to use for the rendering
 * \param (const Texture*) t - The equirectangular texture to use to generate the cubemap
 *
 * \return (Texture*) The generated cubemap
 */
viv::Texture* viv::Skybox::convertToCubeMap(const RenderBackend* backend, Framebuffer *fb, const Texture* t)
{
	//Create a temporary buffer collection to use
	BufferCollection* tmpCollection = backend->createBufferCollection(1, 1, 1);
	//Get buffer pointers
	const VertexBuffer* vbo = tmpCollection->getVertexBuffer(0);
	const IndexBuffer* ebo = tmpCollection->getIndexBuffer(0);
	const UniformBuffer* ubo = tmpCollection->getUniformBuffer(0);

	//Bind the buffer collection, Bind and set the EBO
	tmpCollection->bind();
	ebo->bind();
	ebo->bufferData(this->md->getIndexCount() * sizeof(uint32_t), this->md->getIndices(), GL_STATIC_DRAW);

	//Bind VBO (vertex data) and set it's attributes
	vbo->bind();
	vbo->bufferData(this->md->getVertexCount() * sizeof(Vertex), this->md->getVertices(), GL_STATIC_DRAW);
	vbo->bufferAttribute(AssetManager::getShader("convertToCubeMapShader")->getAttributes()["position"].index, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), 0, 0);
	vbo->unbind();

	tmpCollection->unbind(); //Unbind the buffer collection

	//Bind and set UBO1 (Camera matrices)
	if(ubo != nullptr) {
		ubo->bind();
		ubo->bufferData(2 * sizeof(Mat4), nullptr, GL_DYNAMIC_DRAW);
		ubo->bindBufferBase(AssetManager::getShader("convertToCubeMapShader")->getUniformBlocks()["CamMats"].blockBinding);
		ubo->unbind();
	}

	//Set the shader to use and bind the equirectangular map
	AssetManager::getShader("convertToCubeMapShader")->use();
	t->bind(0);
	AssetManager::getShader("convertToCubeMapShader")->setUniformInt1("equiMap", 0);

	//Capture projection matrix and capture view matrices
	Mat4 captureProjection = glm::perspective(glm::radians(90.0f), 1.0f, 0.1f, 10.0f);
	Mat4 captureViews[] = {
		glm::lookAt(Vec3(0.0f, 0.0f, 0.0f), Vec3( 1.0f,  0.0f,  0.0f), Vec3(0.0f, -1.0f,  0.0f)),
		glm::lookAt(Vec3(0.0f, 0.0f, 0.0f), Vec3(-1.0f,  0.0f,  0.0f), Vec3(0.0f, -1.0f,  0.0f)),
		glm::lookAt(Vec3(0.0f, 0.0f, 0.0f), Vec3( 0.0f,  1.0f,  0.0f), Vec3(0.0f,  0.0f,  1.0f)),
		glm::lookAt(Vec3(0.0f, 0.0f, 0.0f), Vec3( 0.0f, -1.0f,  0.0f), Vec3(0.0f,  0.0f, -1.0f)),
		glm::lookAt(Vec3(0.0f, 0.0f, 0.0f), Vec3( 0.0f,  0.0f,  1.0f), Vec3(0.0f, -1.0f,  0.0f)),
		glm::lookAt(Vec3(0.0f, 0.0f, 0.0f), Vec3( 0.0f,  0.0f, -1.0f), Vec3(0.0f, -1.0f,  0.0f))
	};

	backend->setCullMode(RenderBackend::CullMode::FRONT);

	//For each view: set the ubo matrices, attach the texture, clear the fb, run the cubemap generation shader
	for (uint32_t i = 0; i < 6; i++) {
		if(ubo != nullptr) {
			ubo->bindBufferBase(AssetManager::getShader("convertToCubeMapShader")->getUniformBlocks()["CamMats"].blockBinding);
			ubo->bind();
			ubo->bufferSubData(0, sizeof(Mat4), &captureViews[i]);
			ubo->bufferSubData(sizeof(Mat4), sizeof(Mat4), &captureProjection);
			ubo->unbind();
		}

		fb->attachTexture(this->cubeMap, 0, GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0);
		backend->clear(RenderBackend::ClearMask::COLOR_AND_DEPTH);

		tmpCollection->bind();
		backend->draw(RenderBackend::DrawType::TRIS, this->md.get(), 1);
		tmpCollection->unbind();
	}

	//Cleanup
	t->unbind(0);
	delete tmpCollection;

	backend->setCullMode(RenderBackend::CullMode::BACK);

	return this->cubeMap;
}

/*! \brief Helper function that generates an irradiance map from the skybox cubemap
 *
 * \param (const RenderBacked*) backend - The rendering backend to use
 * \param (Framebuffer*) fb - The framebuffer to use for the rendering
 *
 * \return (Texture*) The generated irradiance map
 */
viv::Texture* viv::Skybox::generateIrradianceMap(const RenderBackend* backend, Framebuffer* fb)
{
	//Create a temporary buffer collection to use
	BufferCollection* tmpCollection = backend->createBufferCollection(1, 1, 1);
	//Get buffer pointers
	const VertexBuffer* vbo = tmpCollection->getVertexBuffer(0);
	const IndexBuffer* ebo = tmpCollection->getIndexBuffer(0);
	const UniformBuffer* ubo = tmpCollection->getUniformBuffer(0);

	//Bind the buffer collection, Bind and set the EBO
	tmpCollection->bind();
	ebo->bind();
	ebo->bufferData(this->md->getIndexCount() * sizeof(uint32_t), this->md->getIndices(), GL_STATIC_DRAW);

	//Bind VBO (vertex data) and set it's attributes
	vbo->bind();
	vbo->bufferData(this->md->getVertexCount() * sizeof(Vertex), this->md->getVertices(), GL_STATIC_DRAW);
	vbo->bufferAttribute(AssetManager::getShader("irradianceShader")->getAttributes()["position"].index, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), 0, 0);
	vbo->unbind();

	tmpCollection->unbind(); //Unbind the buffer collection

	//Bind and set UBO1 (Camera matrices)
	if(ubo != nullptr) {
		ubo->bind();
		ubo->bufferData(2 * sizeof(Mat4), nullptr, GL_DYNAMIC_DRAW);
		ubo->bindBufferBase(AssetManager::getShader("irradianceShader")->getUniformBlocks()["CamMats"].blockBinding);
		ubo->unbind();
	}

	//Set the shader to use and bind the cubemap
	AssetManager::getShader("irradianceShader")->use();
	this->cubeMap->bind(0);
	AssetManager::getShader("irradianceShader")->setUniformInt1("cubeMap", 0);

	//Capture projection matrix and capture view matrices
	Mat4 captureProjection = glm::perspective(glm::radians(90.0f), 1.0f, 0.1f, 10.0f);
	Mat4 captureViews[] = {
		glm::lookAt(Vec3(0.0f, 0.0f, 0.0f), Vec3( 1.0f,  0.0f,  0.0f), Vec3(0.0f, -1.0f,  0.0f)),
		glm::lookAt(Vec3(0.0f, 0.0f, 0.0f), Vec3(-1.0f,  0.0f,  0.0f), Vec3(0.0f, -1.0f,  0.0f)),
		glm::lookAt(Vec3(0.0f, 0.0f, 0.0f), Vec3( 0.0f,  1.0f,  0.0f), Vec3(0.0f,  0.0f,  1.0f)),
		glm::lookAt(Vec3(0.0f, 0.0f, 0.0f), Vec3( 0.0f, -1.0f,  0.0f), Vec3(0.0f,  0.0f, -1.0f)),
		glm::lookAt(Vec3(0.0f, 0.0f, 0.0f), Vec3( 0.0f,  0.0f,  1.0f), Vec3(0.0f, -1.0f,  0.0f)),
		glm::lookAt(Vec3(0.0f, 0.0f, 0.0f), Vec3( 0.0f,  0.0f, -1.0f), Vec3(0.0f, -1.0f,  0.0f))
	};

	backend->setCullMode(RenderBackend::CullMode::FRONT);

	//For each view: set the ubo matrices, attach the texture, clear the fb, run the irradiance shader
	for (uint32_t i = 0; i < 6; i++) {
		if(ubo != nullptr) {
			ubo->bindBufferBase(AssetManager::getShader("irradianceShader")->getUniformBlocks()["CamMats"].blockBinding);
			ubo->bind();
			ubo->bufferSubData(0, sizeof(Mat4), &captureViews[i]);
			ubo->bufferSubData(sizeof(Mat4), sizeof(Mat4), &captureProjection);
			ubo->unbind();
		}

		fb->attachTexture(this->irradianceMap, 0, GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0);
		backend->clear(RenderBackend::ClearMask::COLOR_AND_DEPTH);

		tmpCollection->bind();
		backend->draw(RenderBackend::DrawType::TRIS, this->md.get(), 1);
		tmpCollection->unbind();
	}

	//Cleanup
	this->cubeMap->unbind(0);
	delete tmpCollection;

	backend->setCullMode(RenderBackend::CullMode::BACK);

	return this->irradianceMap;
}

/*! \brief Helper function that generates an prefiltered radiance map from the skybox cubemap
 *
 * \param (const RenderBacked*) backend - The rendering backend to use
 * \param (Framebuffer*) fb - The framebuffer to use for the rendering
 *
 * \return (Texture*) The generated prefilter map
 */
viv::Texture* viv::Skybox::generatePrefilterMap(const RenderBackend* backend, Framebuffer* fb)
{
	BufferCollection* tmpCollection = backend->createBufferCollection(1, 1, 2);
	//Get buffer pointers
	const VertexBuffer* vbo = tmpCollection->getVertexBuffer(0);
	const IndexBuffer* ebo = tmpCollection->getIndexBuffer(0);
	const UniformBuffer* ubo1 = tmpCollection->getUniformBuffer(0);
	const UniformBuffer* ubo2 = tmpCollection->getUniformBuffer(1);

	//Bind the buffer collection, Bind and set the EBO
	tmpCollection->bind();
	ebo->bind();
	ebo->bufferData(this->md->getIndexCount() * sizeof(uint32_t), this->md->getIndices(), GL_STATIC_DRAW);

	//Bind VBO (vertex data) and set it's attributes
	vbo->bind();
	vbo->bufferData(this->md->getVertexCount() * sizeof(Vertex), this->md->getVertices(), GL_STATIC_DRAW);
	vbo->bufferAttribute(AssetManager::getShader("prefilterShader")->getAttributes()["position"].index, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), 0, 0);
	vbo->unbind();

	tmpCollection->unbind(); //Unbind the buffer collection

	//Bind and set UBO1 (Camera matrices)
	if(ubo1 != nullptr) {
		ubo1->bind();
		ubo1->bufferData(2 * sizeof(Mat4), nullptr, GL_DYNAMIC_DRAW);
		ubo1->bindBufferBase(AssetManager::getShader("prefilterShader")->getUniformBlocks()["CamMats"].blockBinding);
		ubo1->unbind();
	}

	if(ubo2 != nullptr) {
		ubo2->bind();
		ubo2->bufferData(sizeof(Vec4), nullptr, GL_DYNAMIC_DRAW);
		ubo2->bindBufferBase(AssetManager::getShader("prefilterShader")->getUniformBlocks()["Roughness"].blockBinding);
		ubo2->unbind();
	}

	//Set the shader to use and bind the cubemap
	AssetManager::getShader("prefilterShader")->use();
	this->cubeMap->bind(0);
	AssetManager::getShader("prefilterShader")->setUniformInt1("cubeMap", 0);

	//Capture projection matrix and capture view matrices
	Mat4 captureProjection = glm::perspective(glm::radians(90.0f), 1.0f, 0.1f, 10.0f);
	Mat4 captureViews[] = {
		glm::lookAt(Vec3(0.0f, 0.0f, 0.0f), Vec3( 1.0f,  0.0f,  0.0f), Vec3(0.0f, -1.0f,  0.0f)),
		glm::lookAt(Vec3(0.0f, 0.0f, 0.0f), Vec3(-1.0f,  0.0f,  0.0f), Vec3(0.0f, -1.0f,  0.0f)),
		glm::lookAt(Vec3(0.0f, 0.0f, 0.0f), Vec3( 0.0f,  1.0f,  0.0f), Vec3(0.0f,  0.0f,  1.0f)),
		glm::lookAt(Vec3(0.0f, 0.0f, 0.0f), Vec3( 0.0f, -1.0f,  0.0f), Vec3(0.0f,  0.0f, -1.0f)),
		glm::lookAt(Vec3(0.0f, 0.0f, 0.0f), Vec3( 0.0f,  0.0f,  1.0f), Vec3(0.0f, -1.0f,  0.0f)),
		glm::lookAt(Vec3(0.0f, 0.0f, 0.0f), Vec3( 0.0f,  0.0f, -1.0f), Vec3(0.0f, -1.0f,  0.0f))
	};

	backend->setCullMode(RenderBackend::CullMode::FRONT);

	//For each mipmap, generate the mipmap data to use
	for (uint32_t mip = 0; mip < 5; mip++) {
		uint32_t mipSize= 128 * std::pow(0.5f, mip);
		fb->resize(mipSize, mipSize);
		backend->resize(mipSize, mipSize);

		float roughness = (float)mip / 4.0f;
		Vec4 roughRes = Vec4(this->cubeMap->getWidth(), this->cubeMap->getHeight(), roughness, roughness);

		//For each view: set the ubo matrices, attach the texture, clear the fb, run the prefilter shader
		for (uint32_t i = 0; i < 6; i++) {
			if(ubo1 != nullptr) {
				ubo1->bindBufferBase(AssetManager::getShader("prefilterShader")->getUniformBlocks()["CamMats"].blockBinding);
				ubo1->bind();
				ubo1->bufferSubData(0, sizeof(Mat4), &captureViews[i]);
				ubo1->bufferSubData(sizeof(Mat4), sizeof(Mat4), &captureProjection);
				ubo1->unbind();
			}

			if(ubo2 != nullptr) {
				ubo2->bindBufferBase(AssetManager::getShader("prefilterShader")->getUniformBlocks()["ResRoughness"].blockBinding);
				ubo2->bind();
				ubo2->bufferSubData(0, sizeof(Vec4), &roughRes);
				ubo2->unbind();
			}

			fb->attachTexture(this->prefilterMap, 0, GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, mip);
			backend->clear(RenderBackend::ClearMask::COLOR_AND_DEPTH);

			tmpCollection->bind();
			backend->draw(RenderBackend::DrawType::TRIS, this->md.get(), 1);
			tmpCollection->unbind();
		}
	}

	//Cleanup
	this->cubeMap->unbind(0);
	delete tmpCollection;
	backend->setCullMode(RenderBackend::CullMode::BACK);

	return prefilterMap;
}

/*! \brief Creats a shader from an equirectangular map
 *
 * \param (const RenderBacked*) backend - The rendering backed to use
 * \param (const std::shared_ptr<Texture> &) tex - The equirectangular texture to use
 */
void viv::Skybox::setTextureFromEquiRect(const RenderBackend* backend, const std::shared_ptr<Texture> &tex)
{
	//Create the cubemap, irradiance map, and the prefilter map
	this->cubeMap = backend->createCubeMap(1024, 1024, 3, 16, GL_FLOAT, GL_CLAMP_TO_EDGE, GL_LINEAR, GL_LINEAR, nullptr, true);
	this->irradianceMap = backend->createCubeMap(32, 32, 3, 16, GL_FLOAT, GL_CLAMP_TO_EDGE, GL_LINEAR, GL_LINEAR, nullptr, false);
	this->prefilterMap = backend->createCubeMap(128, 128, 3, 16, GL_FLOAT, GL_CLAMP_TO_EDGE, GL_LINEAR_MIPMAP_LINEAR, GL_LINEAR, nullptr, true);

	//Run the generation methods on a different framebuffer
	Framebuffer* buffer = backend->createFramebuffer(1024, 1024, true);
	backend->bindFramebuffer(buffer);
	uint32_t attachments = GL_COLOR_ATTACHMENT0;
	buffer->setDrawBuffers(&attachments, 1);
	backend->resize(1024, 1024);
	this->convertToCubeMap(backend, buffer, tex.get());
	buffer->resize(32, 32);
	backend->resize(32, 32);
	this->generateIrradianceMap(backend, buffer);
	buffer->resize(128, 128);
	backend->resize(128, 128);
	this->generatePrefilterMap(backend, buffer);
	backend->bindFramebuffer(nullptr);
	delete buffer;

	//Reset the viewport
	backend->resize(WindowData::innerSize.x, WindowData::innerSize.y);
}

/*! \brief Gets the irradiance map
 *
 * \return (const Texture*) The irradiance map
 */
const viv::Texture* viv::Skybox::getIrradianceMap()
{
	return this->irradianceMap;
}

/*! \brief Gets the prefilter map
 *
 * \return (const Texture*) The prefilter map
 */
const viv::Texture* viv::Skybox::getPrefilterMap()
{
	return this->prefilterMap;
}

/*! \brief Skybox copy assignment operator
 *
 * \param (const Skybox &) sb - The skybox to copy
 *
 * \param (const Skybox &) This skybox after copying
 */
const viv::Skybox & viv::Skybox::operator=(const Skybox &sb)
{
	Drawable::operator=(sb);

	this->cubeMap = sb.cubeMap;
	this->irradianceMap = sb.irradianceMap;
	this->prefilterMap = sb.prefilterMap;

	return *this;
}

/*! \brief Skybox Destructor
 */
viv::Skybox::~Skybox()
{
	delete this->cubeMap;
	delete this->irradianceMap;
	delete this->prefilterMap;
}
