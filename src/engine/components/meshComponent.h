/*! \file meshComponent.h
 * \author Giovanni Aleman
 *
 ******************************************************************************
 * Copyright (c) 2019 Giovanni Aleman
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************
 */

#ifndef MESHCOMPONENT_H
#define MESHCOMPONENT_H

#include "renderComponent.h"

namespace viv
{
	/*! \class MeshComponent
	 *  \brief Holds data for mesh components
	 */
	struct MeshInstanceComponent : RenderComponent
	{
		public:
			std::shared_ptr<Mesh> getMesh();
			void setMesh(std::shared_ptr<Mesh> mesh);
			std::shared_ptr<Shader> getShader();
			void setShader(std::shared_ptr<Shader> shader);

		private:
			std::shared_ptr<Shader> shader = nullptr;
			std::shared_ptr<Mesh> mesh = nullptr;
	};
};

#endif



