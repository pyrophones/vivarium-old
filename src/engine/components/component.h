/*! \file component.h
 * \author Giovanni Aleman
 *
 ******************************************************************************
 * Copyright (c) 2018-2019 Giovanni Aleman
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************
 */

#ifndef COMPONENT_H
#define COMPONENT_H

#include <cstdint>
#include "uid.h"

namespace viv
{
	class Component
	{
		public:
			uint32_t getHandle();
			void setHandle(uint32_t handle);
			const UID & getUID();
			void setUID(UID uid);

		protected:
			uint32_t handle;
			UID id;
			bool needsUpdate;
	};
};

#endif
