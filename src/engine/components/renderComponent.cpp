/*! \file renderComponent.cpp
 * \author Giovanni Aleman
 *
 ******************************************************************************
 * Copyright (c) 2018-2019 Giovanni Aleman
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************
 */

#include "renderComponent.h"

std::shared_ptr<viv::Material> viv::RenderComponent::getMaterial()
{
	return this->material;
}

void viv::RenderComponent::setMaterial(std::shared_ptr<Material> mat)
{
	this->material = mat;
	this->needsUpdate = true;
}