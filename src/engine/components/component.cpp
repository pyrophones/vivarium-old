/*! \file component.cpp
 * \author Giovanni Aleman
 *
 ******************************************************************************
 * Copyright (c) 2018-2019 Giovanni Aleman
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************
 */

uint32_t viv::Component::getHandle()
{
	return this->handle;
}

void viv::Component::setHandle(uint32_t handle)
{
	this->handle = handle;
}

const viv::UID & viv::Component::getUID()
{
	return this->id;
}

void viv::Component::setUID(UID uid)
{
	this->id = uid;
}