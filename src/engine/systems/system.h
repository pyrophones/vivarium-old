/*! \file system.h
 * \author Giovanni Aleman
 *
 ******************************************************************************
 * Copyright (c) 2019 Giovanni Aleman
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************
 */

#ifndef SYSTEM_H
#define SYSTEM_H

#include <string>
#include <vector>
#include <memory>
#include <new>
#include <cstdint>
#include "entityManager.h"
#include "component.h"

namespace viv
{
	/*! \class System
	 *  \brief Class for component systems
	 */
	class System
	{
		public:
			System();
			System(const System &s);
			const System & operator=(const System &s);
			virtual void receiveMessage() = 0;
			virtual void postMessage() = 0;
			virtual void update() = 0;
			virtual ~System();

			//Static methods for as bindings
			//static void DefCtor(Entity* self);
			//static void CopyCtor(const Entity &other, Entity* self);
			//static void InitCtor(std::string tag, char* scriptPath, Entity* self);
			//static void Dtor(Entity* self);

		protected:
			const uint32_t MAX_COMPONENTS = 4096;
			std::vector<Component> componentsMasks;
			std::vector<uint32_t> componentRefs;

	};
}

#endif
