/*! \file renderingSystem.cpp
 * \author Giovanni Aleman
 *
 ******************************************************************************
 * Copyright (c) 2019 Giovanni Aleman
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************
 */

#include "renderingSystem.h"

std::vector<uint32_t> viv::RenderingSystem::componentsToRender = std::vector<uint32_t>();

viv::RenderingSystem::RenderingSystem() : System()
{
	this->componentsToRender.reserve(MAX_COMPONENTS);
}

viv::RenderingSystem::RenderingSystem(const RenderingSystem &rs) : System(rs)
{
	this->componentsToRender = rs.componentsToRender;
}

void viv::RenderingSystem::receiveMessage()
{

}

void viv::RenderingSystem::postMessage()
{

}

void viv::RenderingSystem::update()
{
	for(auto comp : componentsToRender) {

	}
}

void viv::RenderingSystem::draw()
{
	for(auto comp : componentsToRender) {

	}
}

const viv::RenderingSystem & viv::RenderingSystem::operator=(const viv::RenderingSystem &rs)
{
	System::operator=(rs);
	this->componentsToRender = rs.componentsToRender;

	return *this;
}

viv::RenderingSystem::~RenderingSystem()
{

}
