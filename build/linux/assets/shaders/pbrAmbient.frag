#version 450 core

out vec4 fragColor;

in VS_OUT
{
	vec2 uv;
} fsIn;

uniform sampler2D gDepth;
uniform sampler2D gRough;
uniform sampler2D gAlbedoMetal;
uniform sampler2D gNormalAo;
uniform sampler2D brdfLUT;
uniform samplerCube irradianceMap;
uniform samplerCube prefilterMap;

layout (std140) uniform CamProps
{
	mat4 invViewProj;
	vec3 camLoc;
};

const float PI = 3.14159265359;
const float ONE_OVER_PI = 0.318309886184;
const vec3 F0_NON_METAL = vec3(0.04);
const float MAX_REFLECTION_LOD = 4.0;

vec3 fresnelSchlickRoughness(float cosTheta, vec3 f0, float roughness)
{
	return f0 + (max(vec3(1.0 - roughness), f0) - f0) * pow(1.0 - cosTheta, 5.0);
}

void main()
{
	//Retrieve data from gBuffer
	vec3 normal = texture(gNormalAo, fsIn.uv).rgb;
	vec3 albedo = texture(gAlbedoMetal, fsIn.uv).rgb;
	vec3 irradiance = texture(irradianceMap, normal).rgb;
	float depth = texture(gDepth, fsIn.uv).r * 2.0 - 1.0;
	float roughness = texture(gRough, fsIn.uv).r;
	float metallic = texture(gAlbedoMetal, fsIn.uv).a;
	float ao = texture(gNormalAo, fsIn.uv).a;

	vec4 pos = vec4(fsIn.uv * 2.0 - 1.0, depth, 1.0);
	pos = (invViewProj * pos);
	pos /= pos.w;

	//Lighting variables
	vec3 v = normalize(camLoc - pos.xyz);
	vec3 r = reflect(-v, normal);
	float nDotV = max(dot(normal, v), 0.0);

	//Attenuation
	vec3 f0 = mix(F0_NON_METAL, albedo, metallic);

	//Ambient lighting
	vec3 ks = fresnelSchlickRoughness(nDotV, f0, roughness);
	vec3 kd = 1.0 - ks;
	kd *= 1.0 - metallic;

	//Specular
	vec3 prefilteredColor = textureLod(prefilterMap, r, roughness * MAX_REFLECTION_LOD).rgb;
	vec2 envBrdf = texture(brdfLUT, vec2(nDotV, roughness)).rg;
	vec3 spec = prefilteredColor * (ks * envBrdf.x + envBrdf.y);

	vec3 diff = irradiance * albedo;

	vec3 color = (kd * diff + spec) * ao;

	//HDR and gamma correction
	//NOTE: Move these somewhere else
	//color = color / (color + vec3(1.0));
	//color = pow(color, vec3(0.4545454545));

	fragColor = vec4(color, 1.0); //Output the color
}
