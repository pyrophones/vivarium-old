#version 450 core

in VS_OUT
{
	layout (location = 0) flat uint matNum;
	layout (location = 1) vec3 pos;
	layout (location = 2) vec2 uv;
	layout (location = 3) vec3 tint;
	layout (location = 4) vec3 tan;
	layout (location = 5) vec3 bitan;
	layout (location = 6) vec3 normal;
	layout (location = 7) mat3 normalMat;
} fsIn;

layout (location = 0) out float gRough;
layout (location = 1) out vec4 gAlbedoMetal;
layout (location = 2) out vec4 gNormalAo;

const uint MAX_MATERIALS = 5;

layout (std140) uniform ParallaxInfo
{
	vec3 camLoc;
	float parallaxStrength[MAX_MATERIALS];
	float parallaxBias[MAX_MATERIALS];
};

uniform sampler2D albedoTex[MAX_MATERIALS];
uniform sampler2D normalTex[MAX_MATERIALS];
uniform sampler2D metalTex[MAX_MATERIALS];
uniform sampler2D roughTex[MAX_MATERIALS];
uniform sampler2D aoTex[MAX_MATERIALS];
uniform sampler2D dispTex[MAX_MATERIALS];

const float MIN_LAYERS = 8;
const float MAX_LAYERS = 32;

vec2 displacementMapping(sampler2D dispMap, vec2 uv, vec3 view, out float height)
{
	//Displace uvs
	float numLayers = mix(MAX_LAYERS, MIN_LAYERS, abs(dot(vec3(0.0, 0.0, 1.0), view)));
	float layerHeight = 1.0 / numLayers;
	float curLayerHeight = 0.0;
	vec2 p = parallaxStrength[fsIn.matNum] * view.xy / (view.z + parallaxBias[fsIn.matNum]);
	vec2 deltaUvs = p / numLayers;
	vec2 curUvs = uv;
	float curDispMapSample = texture(dispMap, uv).r;

	while((curLayerHeight < curDispMapSample) == true) { // "== true" is necessary to stop infinite loop. Driver bug?
		curLayerHeight += layerHeight;
		curUvs -= deltaUvs;
		curDispMapSample = texture(dispMap, curUvs).r;
	}

	vec2 prevUvs = curUvs + deltaUvs;
	float nextHeight = curDispMapSample - curLayerHeight;
	float prevHeight = texture(dispMap, prevUvs).r - curLayerHeight + layerHeight;
	float weight = nextHeight / (nextHeight - prevHeight);

	height = curLayerHeight + prevHeight * weight + nextHeight * (1.0 - weight);

	return (prevUvs * weight + curUvs * (1.0 - weight));
}

void main()
{
	vec3 n = normalize(fsIn.normalMat * fsIn.normal);
	vec3 t = normalize(fsIn.normalMat * fsIn.tan);
	t = normalize(t - dot(t, n) * n);
	vec3 b = normalize(fsIn.normalMat * fsIn.bitan);
	b = normalize(b - dot(b, n) * n - dot(b, t) * t);

	mat3 tbn = mat3(t, b, n);

	float height;

	mat3 tbnT = transpose(tbn);
	vec3 v = normalize(tbnT * (camLoc - fsIn.pos));
	vec2 uv = displacementMapping(dispTex[fsIn.matNum], fsIn.uv, v, height);

	gRough = texture(roughTex[fsIn.matNum], uv).r; //Roughness

	gAlbedoMetal.rgb = texture(albedoTex[fsIn.matNum], uv).rgb * fsIn.tint; //Diffuse
	gAlbedoMetal.a = texture(metalTex[fsIn.matNum], uv).r; //Metallic

	vec3 norm = normalize(texture(normalTex[fsIn.matNum], uv).rgb * 2.0 - 1.0);
	gNormalAo.rgb = normalize(tbn * norm); //Normal
	gNormalAo.a = texture(aoTex[fsIn.matNum], uv).r; //Ambient Occlusion
}
