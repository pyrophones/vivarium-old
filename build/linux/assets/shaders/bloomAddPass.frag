#version 450

in VS_OUT
{
	vec2 uv;
} vsOut;

out vec4 fragColor;

uniform sampler2D tex1;
uniform sampler2D tex2;

void main()
{
	vec2 texOffset = 1.0 / textureSize(tex2, 0);
	vec4 color1 = texture2D(tex1, fsIn.uv);
	vec4 color2 = texture2D(tex2, fsIn.uv);
	//color2 += texture2D(tex2, gl_TexCoord[0].xy + vec2( 1.0,  0.0) * texOffset);
	//color2 += texture2D(tex2, gl_TexCoord[0].xy + vec2(-1.0,  0.0) * texOffset);
	//color2 += texture2D(tex2, gl_TexCoord[0].xy + vec2( 0.0,  1.0) * texOffset);
	//color2 += texture2D(tex2, gl_TexCoord[0].xy + vec2( 0.0, -1.0) * texOffset);
	//color2 += texture2D(tex2, gl_TexCoord[0].xy + vec2( 1.0,  1.0) * texOffset);
	//color2 += texture2D(tex2, gl_TexCoord[0].xy + vec2(-1.0, -1.0) * texOffset);
	//color2 += texture2D(tex2, gl_TexCoord[0].xy + vec2( 1.0, -1.0) * texOffset);
	//color2 += texture2D(tex2, gl_TexCoord[0].xy + vec2(-1.0,  1.0) * texOffset);
	//color2 /= 9.0;
	fragColor = color1 + color2;
}
