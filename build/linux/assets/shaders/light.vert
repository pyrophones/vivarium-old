#version 450 core

layout (location = 0) in vec3 position;
layout (location = 1) in mat4 worldMat;

layout (std140) uniform CamMats
{
	mat4 view;
	mat4 proj;
};

void main()
{
	gl_Position = proj * view * worldMat * vec4(position, 1.0);
}
