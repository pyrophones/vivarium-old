#version 450 core

in VS_OUT
{
	vec3 uv;
} fsIn;

out vec4 fragColor;

uniform sampler2D equiMap;

const vec2 invAtan = vec2(0.159154943092, 0.318309886184);

vec2 sampleSphericalMap(vec3 v)
{
	vec2 uv = vec2(atan(v.z, v.x), asin(-v.y));
	uv *= invAtan;
	uv += 0.5;
	return uv;
}

void main()
{
	fragColor = vec4(texture(equiMap, sampleSphericalMap(normalize(fsIn.uv))).rgb, 1.0);
}

