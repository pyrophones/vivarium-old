#version 450

in VS_OUT
{
	vec2 uv;
} fsIn;

out vec4 fragColor;

uniform sampler2D tex;

const float kernel[9] = float[](
    0.0625, 0.125, 0.0625,
    0.125,  0.25,  0.125,
    0.0625, 0.125, 0.0625
);

const float offset = 0.0033333333;

void main()
{
	vec2 offsets[9] = vec2[](
		vec2(-offset,  offset), // top-left
		vec2( 0.0f,    offset), // top-center
		vec2( offset,  offset), // top-right
		vec2(-offset,  0.0f),   // center-left
		vec2( 0.0f,    0.0f),   // center-center
		vec2( offset,  0.0f),   // center-right
		vec2(-offset, -offset), // bottom-left
		vec2( 0.0f,   -offset), // bottom-center
		vec2( offset, -offset)  // bottom-right
	);

	vec3 color = vec3(0.0);
	for(int i = 0; i < 9; i++) {
        vec3 samp = vec3(texture(tex, fsIn.uv + offsets[i]));
        color += samp * kernel[i];
    }

	fragColor = vec4(color, 1.0);
}

